﻿using System.Collections.Generic;
using System.Linq;
using ListBoxer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
	[TestClass]
	public class DocumentUnitTests
	{
		private IDocument _document;

		[TestInitialize]
		public void Initialize()
		{
			_document = new Document();
		}

		[TestMethod, Description("Проверка добавления одного элемента")]
		public void AddSimpleTest()
		{
			var expectedElements = new List<Element> {new Element("1")};
			_document.Add(new Element("1"));

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
		}

		[TestMethod, Description("Проверка добавления нескольких элементов")]
		public void AddMultiTest()
		{
			var expectedElements = new List<Element> { new Element("1"), new Element("3"), new Element("2") };
			_document.Add(new Element("1"));
			_document.Add(new Element("3"));
			_document.Add(new Element("2"));

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
		}

		[TestMethod, Description("Проверка удаления элемента")]
		public void SimpleRemoveTest()
		{
			var expectedElements = new List<Element> { new Element("1"), new Element("2") };
			_document.Add(new Element("1"));
			_document.Add(new Element("3"));
			_document.Add(new Element("2"));

			_document.RemoveAt(1);

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
		}

		[TestMethod, Description("Проверка сортировки")]
		public void SimpleSortTest()
		{
			var expectedElements = new List<Element> { new Element("1"), new Element("10"), new Element("2"), new Element("3") };
			_document.Add(new Element("1"));
			_document.Add(new Element("3"));
			_document.Add(new Element("2"));

			_document.AutoSort = true;

			_document.Add(new Element("10"));

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
			Assert.IsTrue(_document.AutoSort);
		}

		[TestMethod, Description("Проверка отмены добавления элемента")]
		public void UndoTest()
		{
			var expectedElements = new List<Element> { new Element("1"), new Element("3") };
			_document.Add(new Element("1"));
			_document.Add(new Element("3"));
			_document.Add(new Element("2"));

			_document.History.Undo();

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
			Assert.IsTrue(_document.History.CanUndo);
			Assert.IsTrue(_document.History.CanRedo);

			expectedElements = new List<Element> { new Element("1"), new Element("3"), new Element("10") };
			_document.Add(new Element("10"));

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
			Assert.IsTrue(_document.History.CanUndo);
			Assert.IsFalse(_document.History.CanRedo);
		}

		[TestMethod, Description("Проверка слияния документов")]
		public void MergeTest()
		{
			var expectedElements = new List<Element> { new Element("1"), new Element("2"), new Element("10"), new Element("11") };
			var mergedElements = new List<Element> { new Element("10"), new Element("11") };

			_document.Add(new Element("1"));
			_document.Add(new Element("2"));

			_document.AddRange(mergedElements);

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
		}

		[TestMethod, Description("Проверка слияния документов с сортировкой")]
		public void MergeWithSortTest()
		{
			var expectedElements = new List<Element> { new Element("1"), new Element("10"), new Element("11"), new Element("2") };
			var mergedElements = new List<Element> { new Element("10"), new Element("11") };

			_document.AutoSort = true;

			_document.Add(new Element("1"));
			_document.Add(new Element("2"));

			_document.AddRange(mergedElements);

			CollectionAssert.AreEqual(expectedElements, _document.ToList());
		}
	}
}
