﻿using System.Collections.Generic;
using ListBoxer;
using ListBoxer.Filtering;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
	[TestClass]
	public class FilterUnitTests
	{
		private IDocument _document;

		public struct FilterEventArgs
		{
			public object Sender;
			public int Index;
			public IFilterElement Element;

			public FilterEventArgs(object sender, int index, IFilterElement element)
			{
				Sender = sender;
				Index = index;
				Element = element;
			}
		}

		[TestInitialize]
		public void Initialize()
		{
			_document = new Document();
		}

		[TestMethod, Description("Фильтр не должен вызвать события удаления при добавление элементов в документ")]
		public void DisabledFilterNotRemoveEventTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("element0"),
				new Element("element1"),
				new Element("element2"),
			};

			var actualEvents = new List<FilterEventArgs>();
			filter.Removed += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));

			_document.AddRange(elements);

			Assert.AreNotEqual(0, filter.Count);
			Assert.AreEqual(0, actualEvents.Count);
		}

		[TestMethod, Description("Фильтр не должен вызвать событие вставки при удалении элементов из документа")]
		public void DisabledFilterNotInsertedEventTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("element0"),
				new Element("element1"),
				new Element("element2"),
			};

			_document.AddRange(elements);

			Assert.AreNotEqual(0, filter.Count);

			var actualEvents = new List<FilterEventArgs>();
			filter.Inserted += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));
			for (var i = _document.Count - 1; i >= 0; i--)
				_document.RemoveAt(i);

			Assert.AreEqual(0, actualEvents.Count);
		}

		[TestMethod, Description("Включение фильтра не должено вызвать события добавления элементов")]
		public void EnablingFilterNotInsertEventTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
			};

			_document.AddRange(elements);

			var actualEvents = new List<FilterEventArgs>();
			filter.Inserted += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));
			filter.Pattern = "^[0-9]+$";

			Assert.AreNotEqual(0, filter.Count);
			Assert.AreEqual(0, actualEvents.Count);
		}

		[TestMethod, Description("Выключение фильтра не должено вызвать события удаления элементов")]
		public void DisablingFilterNotRemoveEventTest()
		{
			var filter = new PcreFilter(_document, "^[0-9]+$");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
			};

			_document.AddRange(elements);

			Assert.AreNotEqual(0, filter.Count);

			var actualEvents = new List<FilterEventArgs>();
			filter.Removed += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));
			filter.Pattern = ".+";

			Assert.AreEqual(0, actualEvents.Count);
		}
	}
}
