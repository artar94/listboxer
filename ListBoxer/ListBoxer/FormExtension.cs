﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ListBoxer
{
	public static class FormExtension
	{
		[Flags]
		public enum WindowPlacementFlags : uint
		{
			WpfAsyncWindowPlacement = 0x0004,
			WpfRestoreToMaximized = 0x0002,
			WpfSetMinPosition = 0x0001,
		}

		public enum ShowWindowCommands : uint
		{
			Hide = 0,
			Normal = 1,
			Minimized = 2,
			Maximized = 3,
		}

		[Serializable]
		[StructLayout(LayoutKind.Sequential)]
		public struct WindowPlacement
		{
			public int Length;
			public int Flags;
			public ShowWindowCommands ShowCommands;
			public Point MinPosition;
			public Point MaxPosition;

			private readonly Win32Rect rcNormalPosition;

			public Rectangle NormalPosition => rcNormalPosition;
		}

		[Serializable]
		[StructLayout(LayoutKind.Sequential)]
		public struct Win32Rect
		{
			public int Left;
			public int Top;
			public int Right;
			public int Bottom;

			public Win32Rect(int left, int top, int right, int bottom)
			{
				Left = left;
				Top = top;
				Right = right;
				Bottom = bottom;
			}

			public static implicit operator Rectangle(Win32Rect rect)
			{
				return new Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
			}

			public static implicit operator Win32Rect(Rectangle rect)
			{
				return new Win32Rect(rect.X, rect.Y, rect.Right, rect.Bottom);
			}
		}

		public static WindowPlacement GetPlacement(this Form @this)
		{
			return GetPlacement(@this.Handle);
		}

		public static WindowPlacement GetPlacement(IntPtr hwnd)
		{
			var placement = new WindowPlacement
			{
				Length = Marshal.SizeOf(typeof (WindowPlacement))
			};
			if (GetWindowPlacement(hwnd, ref placement))
				return placement;

			throw new Win32Exception();
		}

		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool GetWindowPlacement(IntPtr hWnd, ref WindowPlacement lpwndpl);
	}
}