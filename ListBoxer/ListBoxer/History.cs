﻿using System;
using System.Collections.Generic;

namespace ListBoxer
{
	public delegate void HistoryEvent(object sender);

	public class History
	{
		const int ForceModifiedIndex = -2;

		private int _currentIndex = -1;
		private int _unmodifiedIndex = -1;
		private readonly List<HistoryItem> _items = new List<HistoryItem>();
		private readonly WeakReference<IDirectDocument> _document;

		public bool CanUndo => _currentIndex >= 0;
		public bool CanRedo => _items.Count > 0 && _currentIndex < _items.Count - 1;
		public int Count => _items.Count;
		public bool IsModified
		{
			get { return _currentIndex != _unmodifiedIndex; }
			set
			{
				if (value == IsModified) return;
				_unmodifiedIndex = value ? ForceModifiedIndex : _currentIndex;
				Changed?.Invoke(this);
			}
		}

		public event HistoryEvent Changed;

		public History(IDirectDocument document)
		{
			_document = new WeakReference<IDirectDocument>(document);
		}

		private void CutOffHistory()
		{
			var index = _currentIndex + 1;
			if (index < Count)
				_items.RemoveRange(index, Count - index);

			if (_unmodifiedIndex > _currentIndex)
				_unmodifiedIndex = ForceModifiedIndex;
		}

		public void Undo()
		{
			if (!CanUndo) return;

			IDirectDocument document;
			if (!_document.TryGetTarget(out document)) return;

			_items[_currentIndex].Undo(document);
			_currentIndex--;

			Changed?.Invoke(this);
		}

		public void Redo()
		{
			if (!CanRedo) return;

			IDirectDocument document;
			if (!_document.TryGetTarget(out document)) return;

			_currentIndex++;
			_items[_currentIndex].Redo(document);

			Changed?.Invoke(this);
		}

		public void Add(HistoryItem item)
		{
			IDirectDocument document;
			if (!_document.TryGetTarget(out document)) return;

			CutOffHistory();
			_items.Add(item);
			_currentIndex++;

			item.Redo(document);

			Changed?.Invoke(this);
		}
	}
}
