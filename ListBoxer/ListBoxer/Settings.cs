﻿using System.IO;
using System.Windows.Forms;
using PeanutButter.INIFile;

namespace ListBoxer
{
	public class Settings
	{
		private readonly INIFile _ini;

		public string FileName { get; private set; }

		public bool AllowResize
		{
			get { return GetBool("Window", "AllowResize", true); }
			set { SetBool("Window", "AllowResize", value); }
		}

		public int LocationX
		{
			get { return GetInt("Window", "LocationX", 0); }
			set { SetInt("Window", "LocationX", value); }
		}

		public int LocationY
		{
			get { return GetInt("Window", "LocationY", 0); }
			set { SetInt("Window", "LocationY", value); }
		}

		public int SizeX
		{
			get { return GetInt("Window", "SizeX", 410); }
			set { SetInt("Window", "SizeX", value); }
		}

		public int SizeY
		{
			get { return GetInt("Window", "SizeY", 350); }
			set { SetInt("Window", "SizeY", value); }
		}

		public FormWindowState WindowState
		{
			get
			{
				var state = (FormWindowState) GetInt("Window", "WindowState", (int) FormWindowState.Normal);
				switch (state)
				{
					case FormWindowState.Normal:
						return state;
					case FormWindowState.Minimized:
						return state;
					case FormWindowState.Maximized:
						return state;
					default:
						return FormWindowState.Normal;
				}
			}
			set { SetInt("Window", "WindowState", (int) value); }
		}

		public FormStartPosition WindowStartPosition
		{
			get
			{
				if (_ini.HasSetting("Window", "LocationX") &&
					_ini.HasSetting("Window", "LocationY") &&
				    _ini.HasSetting("Window", "SizeX") &&
					_ini.HasSetting("Window", "SizeY"))
					return FormStartPosition.Manual;
				
				return FormStartPosition.CenterScreen;
			}
		}

		public bool VerboseMode
		{
			get { return GetBool("Behavior", "VerboseMode", false); }
			set { SetBool("Behavior", "VerboseMode", value); }
		}

		public Settings()
		{
			FileName = "";
			_ini = new INIFile();
		}

		public Settings(string fileName)
		{
			FileName = fileName;
			_ini = new INIFile();
			if (!File.Exists(FileName)) return;

			try
			{
				_ini.Load(FileName);
			}
			catch
			{
				// ignored
			}
		}

		public void Save()
		{
			if (!_ini.HasSetting("Behavior", "VerboseMode"))
				SetBool("Behavior", "VerboseMode", false);

			if (!_ini.HasSetting("Window", "AllowResize"))
				SetBool("Window", "AllowResize", true);

			try
			{
				_ini.Persist(FileName);
			}
			catch
			{
				// ignored
			}
		}

		public void SaveToFile(string fileName)
		{
			FileName = fileName;
			Save();
		}

		private int GetInt(string section, string key, int defaultValue)
		{
			try
			{
				return int.Parse(_ini.GetValue(section, key));
			}
			catch
			{
				return defaultValue;
			}
		}

		private void SetInt(string section, string key, int value)
		{
			_ini.SetValue(section, key, value.ToString());
		}

		private bool GetBool(string section, string key, bool defaultValue)
		{
			try
			{
				return bool.Parse(_ini.GetValue(section, key));
			}
			catch
			{
				return defaultValue;
			}
		}

		private void SetBool(string section, string key, bool value)
		{
			_ini.SetValue(section, key, value.ToString());
		}
	}
}