﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ListBoxer.Filtering;
using ListBoxer.HistoryItems;
using Szotar.WindowsForms;

namespace ListBoxer
{
	public partial class MainWindow : Form
	{
		private IDocument _document;

		private readonly Settings _setting =
			new Settings(Path.ChangeExtension(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName, ".ini"));

		public IDocument Document
		{
			get
			{
				return _document;
			}
			set
			{
				_document = value;

				Filter = new PcreFilter(_document, @"^.+$");

				Filter.Inserted += FilterOnInserted;
				Filter.Removed += FilterOnRemoved;

				_document.Sorted += DocumentOnSorted;
				_document.Saved += DocumentOnSaved;
				_document.History.Changed += HistoryOnChanged;

				listBoxDocument.BeginUpdate();
				try
				{
					listBoxDocument.Items.Clear();
					Filter.ToList().ForEach(e => listBoxDocument.Items.Add(e.Element));
				}
				finally
				{
					listBoxDocument.EndUpdate();
				}

				textBoxFilter.Text = Filter.Pattern;
				textBoxElement.Text = "";

				checkBoxSort.CheckedChanged -= checkBoxSort_CheckedChanged;
				checkBoxSort.Checked = _document.AutoSort;
				checkBoxSort.CheckedChanged += checkBoxSort_CheckedChanged;

				UpdateMainMenu();
				UpdateCaption();
				UpdateStatusBar();
			}
		}

		public IFilter Filter { get; private set; }

		private bool _inTextBox;
		private bool InTextBox
		{
			get { return _inTextBox; }
			set
			{
				_inTextBox = value;
				UpdateMainMenu();
			}
		}

		private bool _inDocument;
		private bool InDocument
		{
			get { return _inDocument; }
			set
			{
				_inDocument = value;
				UpdateMainMenu();
			}
		}

		public MainWindow()
		{
			InitializeComponent();
			LoadWindowSettings();

			Document = new Document();

			mainMenu.Renderer = new ToolStripAeroRenderer(ToolbarTheme.Toolbar);
			mainMenuItemExit.ShortcutKeyDisplayString = @"Esc";

			textBoxElement.Enter += (sender, args) => InTextBox = true;
			textBoxElement.Leave += (sender, args) => InTextBox = false;

			textBoxFilter.Enter += (sender, args) => InTextBox = true;
			textBoxFilter.Leave += (sender, args) => InTextBox = false;

			listBoxDocument.Enter += (sender, args) => { InDocument = true;};
			listBoxDocument.Leave += (sender, args) => InDocument = false;
		}

		private void LoadWindowSettings()
		{
			WindowState = _setting.WindowState == FormWindowState.Minimized ? FormWindowState.Normal : _setting.WindowState;
			StartPosition = _setting.WindowStartPosition;
			if (StartPosition != FormStartPosition.Manual) return;

			var bounds = new Rectangle(_setting.LocationX, _setting.LocationY, _setting.SizeX, _setting.SizeY);
			var screen = Screen.FromRectangle(bounds);

			if (bounds.Right > screen.WorkingArea.Right) bounds.X = screen.WorkingArea.Right - bounds.Width;
			if (bounds.X < screen.WorkingArea.X) bounds.X = screen.WorkingArea.X;
			if (bounds.Right > screen.WorkingArea.Right) bounds.Width = screen.WorkingArea.Right - bounds.X;

			if (bounds.Bottom > screen.WorkingArea.Bottom) bounds.Y = screen.WorkingArea.Bottom - bounds.Height;
			if (bounds.Y < screen.WorkingArea.Y) bounds.Y = screen.WorkingArea.Y;
			if (bounds.Bottom > screen.WorkingArea.Bottom) bounds.Height = screen.WorkingArea.Bottom - bounds.Y;

			if (bounds.Width < MinimumSize.Width) bounds.Width = MinimumSize.Width;
			if (bounds.Height < MinimumSize.Height) bounds.Height = MinimumSize.Height;

			Bounds = bounds;

			FormBorderStyle = _setting.AllowResize ? FormBorderStyle.Sizable : FormBorderStyle.FixedSingle;
			MaximizeBox = _setting.AllowResize;
			statusBar.SizingGrip = _setting.AllowResize;
		}

		private void SaveWindowSettings()
		{
			_setting.WindowState = WindowState;
			var bounds = WindowState == FormWindowState.Normal ? Bounds : this.GetPlacement().NormalPosition;
			_setting.LocationX = bounds.X;
			_setting.LocationY = bounds.Y;
			_setting.SizeX = bounds.Width;
			_setting.SizeY = bounds.Height;
		}

		private void UpdateMainMenu()
		{
			mainMenuItemUndo.Enabled = Document.History.CanUndo & !InTextBox;
			mainMenuItemRedo.Enabled = Document.History.CanRedo & !InTextBox;
			mainMenuItemCut.Enabled = (listBoxDocument.SelectedIndex >= 0) & InDocument;
			mainMenuItemCopy.Enabled = (listBoxDocument.SelectedIndex >= 0) & InDocument;
			mainMenuItemDelete.Enabled = (listBoxDocument.SelectedIndex >= 0) & InDocument;
			mainMenuItemClean.Enabled = Document.Count > 0;
			mainMenuItemDeleteMask.Enabled = Filter.Count > 0;
		}

		private void UpdateCaption()
		{
			var builder = new StringBuilder();
			if (Document.FileName != "")
				builder.Append($"{Path.GetFileNameWithoutExtension(Document.FileName)} - ");
			builder.Append(AboutBox.AssemblyTitle);
			Text = builder.ToString();
		}

		private void UpdateStatusBar()
		{
			statusBarLabelElementCount.Text = $"Количество элементов: {Document.Count}";
			statusBarLabelFilterCount.Text = $"Отфильтровано: {Filter.Count}";
			statusBarLabelModified.Text = Document.History.IsModified ? @"Изменён" : "";
		}

		private bool SaveAs()
		{
			if (saveFileDialog.ShowDialog(this) != DialogResult.OK) return false;
			var fileName = saveFileDialog.FileName;

			try
			{
				Document.SaveToFile(fileName);
				return true;
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся сохранить файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		private bool Save()
		{
			if (Document.FileName == "") return SaveAs();

			try
			{
				Document.SaveToFile(Document.FileName);
				return true;
			}
			catch
			{
				return SaveAs();
			}
		}

		private bool IsReplacementPossible()
		{
			if (!Document.History.IsModified) return true;

			// ReSharper disable once SwitchStatementMissingSomeCases
			switch (MessageBox.Show(this, @"Документ был изменён. Сохранить?", AboutBox.AssemblyTitle,
				MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning))
			{
				case DialogResult.Cancel:
					return false;
				case DialogResult.Yes:
					return Save();
				default:
					return true;
			}
		}

		private void DeleteFromIndex(int index)
		{
			if (_setting.VerboseMode)
				if (MessageBox.Show(this, $"Вы действительно хотите удалить элемент \"{Filter[index].Element}\"?",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
					return;

			Document.RemoveAt(Filter[index].DocumentIndex);

			if (index < listBoxDocument.Items.Count)
				listBoxDocument.SelectedIndex = index;
			else if (index - 1 >= 0)
				listBoxDocument.SelectedIndex = index - 1;
		}

		private void FilterOnInserted(object sender, int index, IFilterElement element)
		{
			listBoxDocument.Items.Insert(index, element.Element);

			if (listBoxDocument.TopIndex > index)
				listBoxDocument.TopIndex = index;
			else if (listBoxDocument.TopIndex + (listBoxDocument.ClientSize.Height/listBoxDocument.ItemHeight) <= index)
				listBoxDocument.TopIndex = index - (listBoxDocument.ClientSize.Height/listBoxDocument.ItemHeight) + 1;

			UpdateStatusBar();
			UpdateMainMenu();
		}

		private void FilterOnRemoved(object sender, int index, IFilterElement element)
		{
			listBoxDocument.Items.RemoveAt(index);

			UpdateStatusBar();
			UpdateMainMenu();
		}

		private void HistoryOnChanged(object sender)
		{
			UpdateMainMenu();
			UpdateStatusBar();
		}

		private void DocumentOnSorted(object sender, bool sorted)
		{
			checkBoxSort.CheckedChanged -= checkBoxSort_CheckedChanged;
			checkBoxSort.Checked = sorted;
			checkBoxSort.CheckedChanged += checkBoxSort_CheckedChanged;
		}

		private void DocumentOnSaved(object sender, string fileName)
		{
			UpdateCaption();
		}

		private void buttonAdd_Click(object sender, EventArgs e)
		{
			try
			{
				errorProvider.Clear();
				Document.Add(new Element(textBoxElement.Text));
				textBoxElement.Text = "";
			}
			catch (ArgumentOutOfRangeException)
			{
				errorProvider.SetIconPadding(textBoxElement, Convert.ToInt32(-20.0*AutoScaleFactor.Width));
				errorProvider.SetError(textBoxElement, @"Строка имеет недопустимую длину");
			}
			catch (ArgumentException)
			{
				errorProvider.SetIconPadding(textBoxElement, Convert.ToInt32(-20.0*AutoScaleFactor.Width));
				errorProvider.SetError(textBoxElement, @"Строка содержит недопустимый символ");
			}
			catch (Exception exception)
			{
				errorProvider.SetIconPadding(textBoxElement, Convert.ToInt32(-20.0*AutoScaleFactor.Width));
				errorProvider.SetError(textBoxElement, exception.Message);
			}
		}

		private void checkBoxSort_CheckedChanged(object sender, EventArgs e)
		{
			if (((CheckBox) sender).Checked)
			{
				listBoxDocument.BeginUpdate();
				try
				{
					Document.AutoSort = true;
				}
				finally
				{
					listBoxDocument.EndUpdate();
				}
			}
			else
				Document.AutoSort = false;
		}

		private void textBoxFilter_TextChanged(object sender, EventArgs e)
		{
			try
			{
				errorProvider.Clear();
				listBoxDocument.BeginUpdate();
				try
				{
					Filter.Pattern = ((TextBox)sender).Text;
				}
				finally
				{
					listBoxDocument.EndUpdate();
				}	
			}
			catch (Exception exception)
			{
				errorProvider.SetIconPadding((Control) sender, Convert.ToInt32(-20.0*AutoScaleFactor.Width));
				errorProvider.SetError((Control) sender, exception.Message);
			}
		}

		private void MainWindow_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Escape)
				Close();
		}

		private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_setting.VerboseMode)
				if (MessageBox.Show(this, $"Закрыть {AboutBox.AssemblyTitle}?", AboutBox.AssemblyTitle,
					MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
				{
					e.Cancel = true;
					return;
				}

			if (!IsReplacementPossible())
				e.Cancel = true;
		}

		private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
		{
			SaveWindowSettings();
			_setting.Save();
		}

		private void toolStripMenuItemExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void mainMenuItemUndo_Click(object sender, EventArgs e)
		{
			Document.History.Undo();
		}

		private void mainMenuItemRedo_Click(object sender, EventArgs e)
		{
			Document.History.Redo();
		}

		private void mainMenuItemCut_Click(object sender, EventArgs e)
		{
			if (listBoxDocument.SelectedIndex < 0) return;

			Clipboard.SetText(Filter[listBoxDocument.SelectedIndex].Element.Value);
			DeleteFromIndex(listBoxDocument.SelectedIndex);
		}

		private void mainMenuItemCopy_Click(object sender, EventArgs e)
		{
			if (listBoxDocument.SelectedIndex < 0) return;

			Clipboard.SetText(Filter[listBoxDocument.SelectedIndex].Element.Value);
		}

		private void mainMenuItemDelete_Click(object sender, EventArgs e)
		{
			if (listBoxDocument.SelectedIndex < 0)
				return;

			DeleteFromIndex(listBoxDocument.SelectedIndex);
		}

		private void mainMenuItemClean_Click(object sender, EventArgs e)
		{
			if (_setting.VerboseMode)
				if (MessageBox.Show(this, @"Вы действительно хотите удалить все элементы?", AboutBox.AssemblyTitle,
					MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
					return;

			Document.Clear();
		}

		private void mainMenuItemDeleteMask_Click(object sender, EventArgs e)
		{
			if (_setting.VerboseMode)
				if (MessageBox.Show(this, @"Вы действительно хотите удалить отфильтрованные элементы?", AboutBox.AssemblyTitle,
					MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
					return;

			var cleanHistoryItem = new CompositeHistoryItem();
			for (var i = Filter.Count - 1; i >= 0; i--)
				cleanHistoryItem.Add(new RemoveHistoryItem(Filter[i].DocumentIndex));
			Document.History.Add(cleanHistoryItem);
		}

		private void textBoxElement_TextChanged(object sender, EventArgs e)
		{
			errorProvider.Clear();
		}

		private void listBoxDocument_SelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateMainMenu();
		}

		private void listBoxDocument_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			var index = ((ListBox)sender).SelectedIndex;
			if (index < 0) return;

			textBoxElement.Text = Filter[index].Element.Value;
		}

		private void mainMenuItemNew_Click(object sender, EventArgs e)
		{
			if (!IsReplacementPossible()) return;

			Document = new Document();
		}

		private void mainMenuItemOpen_Click(object sender, EventArgs e)
		{
			if (!IsReplacementPossible()) return;

			if (openFileDialog.ShowDialog(this) != DialogResult.OK) return;
			var fileName = openFileDialog.FileName;

			try
			{
				Document = new Document(fileName);
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void mainMenuItemSaveAs_Click(object sender, EventArgs e)
		{
			SaveAs();
		}

		private void mainMenuItemSave_Click(object sender, EventArgs e)
		{
			Save();
		}

		private void mainMenuItemMerge_Click(object sender, EventArgs e)
		{
			if (openFileDialog.ShowDialog(this) != DialogResult.OK) return;
			var fileName = openFileDialog.FileName;

			try
			{
				var document = new Document(fileName);
				Document.AddRange(document.ToList());
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void mainMenuItemAbout_Click(object sender, EventArgs e)
		{
			new AboutBox().ShowDialog(this);
		}

		private void listBoxDocument_DrawItem(object sender, DrawItemEventArgs e)
		{
			if (e.Index < 0) return;

			var listBox = (ListBox)sender;
			e.DrawBackground();

			var textRect = new Rectangle(e.Bounds.X + 1, e.Bounds.Y, e.Bounds.Width - 2, e.Bounds.Height);

			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
				e.Graphics.FillRectangle(new SolidBrush(InDocument ? e.BackColor : Color.LightGray), e.Bounds);

			TextRenderer.DrawText(e.Graphics, listBox.Items[e.Index].ToString(),
				e.Font, textRect, InDocument ? e.ForeColor : ForeColor,
				TextFormatFlags.SingleLine | TextFormatFlags.Left | TextFormatFlags.VerticalCenter);
			e.DrawFocusRectangle();
		}

		private void mainMenuItemManual_Click(object sender, EventArgs e)
		{
			Help.ShowHelp(this, "ListBoxer.chm");
		}

		private void MainWindow_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Copy;
		}

		private void MainWindow_DragDrop(object sender, DragEventArgs e)
		{
			var files = (string[]) e.Data.GetData(DataFormats.FileDrop);
			if (files.Length <= 0) return;

			Activate();

			if (!IsReplacementPossible()) return;
			var fileName = files[0];
			try
			{
				Document = new Document(fileName);
			}
			catch (ArgumentOutOfRangeException)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\nСтрока имеет недопустимую длину",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch (ArgumentException)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\nСтрока содержит недопустимый символ",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
