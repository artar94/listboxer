﻿using System.Collections.Generic;

namespace ListBoxer
{
	public abstract class HistoryItem
	{
		public abstract void Undo(IDirectDocument document);
		public abstract void Redo(IDirectDocument document);
	}

	public class CompositeHistoryItem : HistoryItem
	{
		private readonly List<HistoryItem> _items = new List<HistoryItem>();

		public void Add(HistoryItem item)
		{
			_items.Add(item);
		}

		public override void Undo(IDirectDocument document)
		{
			for (var i = _items.Count - 1; i >= 0; i--)
				_items[i].Undo(document);
		}

		public override void Redo(IDirectDocument document)
		{
			var count = _items.Count;
			for (var i = 0; i < count; i++)
				_items[i].Redo(document);
		}
	}
}