using System;

namespace ListBoxer.Filtering
{
	public interface IFilterElement: IEquatable<IFilterElement>
	{
		Element Element { get; }
		int DocumentIndex { get; }
	}
}