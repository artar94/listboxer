﻿using System;

namespace ListBoxer.Filtering
{
	public class PcreFilterElement: IFilterElement
	{
		public Element Element { get; }

		private int _documentIndex;
		public int DocumentIndex
		{
			get { return _documentIndex; }
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), @"Индекс в документе не может быть отрицательным");
				_documentIndex = value;
			}
		}

		public PcreFilterElement(Element element, int documentIndex)
		{
			if (element == null)
				throw new ArgumentNullException(nameof(element), @"Элемент не может быть null");
			if (documentIndex < 0)
				throw new ArgumentOutOfRangeException(nameof(documentIndex), @"Индекс в документе не может быть отрицательным");

			Element = element;
			DocumentIndex = documentIndex;
		}

		public bool Equals(IFilterElement other)
		{
			if (ReferenceEquals(this, other))
				return true;

			return (Element == other.Element) && (DocumentIndex == other.DocumentIndex);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((IFilterElement) obj);
		}

		protected bool Equals(PcreFilterElement other)
		{
			return Equals((IFilterElement) other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (Element?.GetHashCode() ?? 0) * 397;
			}
		}

		public override string ToString()
		{
			return $"{DocumentIndex}: {Element}";
		}
	}
}