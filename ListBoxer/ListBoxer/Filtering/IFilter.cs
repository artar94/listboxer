﻿using System.Collections.Generic;

namespace ListBoxer.Filtering
{
	public delegate void FilterChangeEvent(object sender, int index, IFilterElement element);

	public interface IFilter: IReadOnlyList<IFilterElement>
	{
		string Pattern { get; set; }

		event FilterChangeEvent Inserted;
		event FilterChangeEvent Removed;
	}
}