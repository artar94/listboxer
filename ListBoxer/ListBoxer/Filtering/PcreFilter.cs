﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PCRE;

namespace ListBoxer.Filtering
{
	public class PcreFilter: IFilter
	{
		private readonly IDocument _document;
		private PcreRegex _regex;
		private readonly List<PcreFilterElement> _elements = new List<PcreFilterElement>(); 

		public string Pattern
		{
			get { return _regex.PaternInfo.PatternString; }
			set
			{
				if (_regex?.PaternInfo.PatternString.Equals(value) ?? false) return;

				_regex = new PcreRegex(value, PcreOptions.Compiled | PcreOptions.Singleline | PcreOptions.Unicode);

				var index = 0;
				while (index < _elements.Count)
				{
					if (!_regex.IsMatch(_elements[index].Element.Value))
					{
						var removedElement = _elements[index];
						_elements.RemoveAt(index);
						Removed?.Invoke(this, index, removedElement);
					}
					else
					{
						index++;
					}
				}

				for (var i = 0; i < _document.Count; i++)
					if (_regex.IsMatch(_document[i].Value))
					{
						if (_elements.Exists(e => e.DocumentIndex == i))
							continue;

						var filterIndex = _elements.FindIndex(x => x.DocumentIndex > i);
						if (filterIndex < 0)
							filterIndex = _elements.Count;

						_elements.Insert(filterIndex, new PcreFilterElement(_document[i], i));
						Inserted?.Invoke(this, filterIndex, _elements[filterIndex]);
					}
			}
		}

		public int Count => _elements.Count;

		public event FilterChangeEvent Inserted;
		public event FilterChangeEvent Removed;

		public PcreFilter(IDocument document, string pattern)
		{
			if (document == null)
				throw new ArgumentNullException(nameof(document), @"Document can not be null");

			if (pattern == null)
				throw new ArgumentNullException(nameof(pattern), @"Pattern string can not be null");

			_document = document;
			_document.Inserted += DocumentOnInserted;
			_document.Removed += DocumentOnRemoved;

			Pattern = pattern;
		}
		public IFilterElement this[int index] => _elements[index];

		public IEnumerator<IFilterElement> GetEnumerator()
		{
			return _elements.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private void DocumentOnInserted(object sender, int index, Element element)
		{
			_elements.Where(e => e.DocumentIndex >= index).ToList().ForEach(e => e.DocumentIndex++);

			if (!_regex.IsMatch(element.Value))
				return;

			var filterIndex = _elements.FindIndex(x => x.DocumentIndex > index);
			if (filterIndex < 0)
				filterIndex = _elements.Count;

			_elements.Insert(filterIndex, new PcreFilterElement(element, index));
			Inserted?.Invoke(this, filterIndex, _elements[filterIndex]);
		}

		private void DocumentOnRemoved(object sender, int index, Element element)
		{
			var filterIndex = _elements.FindIndex(x => x.DocumentIndex == index);
			if (filterIndex >= 0)
			{

				var removedElement = _elements[filterIndex];
				_elements.RemoveAt(filterIndex);
				Removed?.Invoke(this, filterIndex, removedElement);
			}

			_elements.Where(e => e.DocumentIndex > index).ToList().ForEach(e => e.DocumentIndex--);
		}
	}
}