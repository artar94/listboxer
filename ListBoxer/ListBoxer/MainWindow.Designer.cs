﻿using System.ComponentModel;
using System.Windows.Forms;

namespace ListBoxer
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.listBoxDocument = new System.Windows.Forms.ListBox();
			this.textBoxFilter = new System.Windows.Forms.TextBox();
			this.mainMenu = new System.Windows.Forms.MenuStrip();
			this.mainMenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemNew = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.mainMenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.mainMenuItemMerge = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.mainMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemUndo = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemRedo = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.mainMenuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemClean = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemDeleteMask = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemManual = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.mainMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.labelFilter = new System.Windows.Forms.Label();
			this.checkBoxSort = new System.Windows.Forms.CheckBox();
			this.buttonAdd = new System.Windows.Forms.Button();
			this.textBoxElement = new System.Windows.Forms.TextBox();
			this.labelElement = new System.Windows.Forms.Label();
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.statusBar = new System.Windows.Forms.StatusStrip();
			this.statusBarLabelElementCount = new System.Windows.Forms.ToolStripStatusLabel();
			this.statusBarLabelFilterCount = new System.Windows.Forms.ToolStripStatusLabel();
			this.statusBarLabelModified = new System.Windows.Forms.ToolStripStatusLabel();
			this.mainMenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuItemCut = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenu.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			this.statusBar.SuspendLayout();
			this.SuspendLayout();
			// 
			// listBoxDocument
			// 
			this.listBoxDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.listBoxDocument.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.listBoxDocument.FormattingEnabled = true;
			this.listBoxDocument.ItemHeight = 15;
			this.listBoxDocument.Location = new System.Drawing.Point(12, 59);
			this.listBoxDocument.Name = "listBoxDocument";
			this.listBoxDocument.Size = new System.Drawing.Size(365, 184);
			this.listBoxDocument.TabIndex = 5;
			this.listBoxDocument.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBoxDocument_DrawItem);
			this.listBoxDocument.SelectedIndexChanged += new System.EventHandler(this.listBoxDocument_SelectedIndexChanged);
			this.listBoxDocument.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxDocument_MouseDoubleClick);
			// 
			// textBoxFilter
			// 
			this.textBoxFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.errorProvider.SetIconPadding(this.textBoxFilter, -20);
			this.textBoxFilter.Location = new System.Drawing.Point(75, 251);
			this.textBoxFilter.Name = "textBoxFilter";
			this.textBoxFilter.Size = new System.Drawing.Size(302, 23);
			this.textBoxFilter.TabIndex = 7;
			this.textBoxFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
			// 
			// mainMenu
			// 
			this.mainMenu.AutoSize = false;
			this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuItemFile,
            this.mainMenuItemEdit,
            this.mainMenuItemHelp});
			this.mainMenu.Location = new System.Drawing.Point(0, 0);
			this.mainMenu.Name = "mainMenu";
			this.mainMenu.Padding = new System.Windows.Forms.Padding(0);
			this.mainMenu.Size = new System.Drawing.Size(389, 20);
			this.mainMenu.TabIndex = 0;
			// 
			// mainMenuItemFile
			// 
			this.mainMenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuItemNew,
            this.mainMenuItemOpen,
            this.mainMenuSeparator1,
            this.mainMenuItemSave,
            this.mainMenuItemSaveAs,
            this.mainMenuSeparator2,
            this.mainMenuItemMerge,
            this.mainMenuSeparator5,
            this.mainMenuItemExit});
			this.mainMenuItemFile.Name = "mainMenuItemFile";
			this.mainMenuItemFile.Size = new System.Drawing.Size(48, 20);
			this.mainMenuItemFile.Text = "&Файл";
			// 
			// mainMenuItemNew
			// 
			this.mainMenuItemNew.Name = "mainMenuItemNew";
			this.mainMenuItemNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.mainMenuItemNew.Size = new System.Drawing.Size(225, 22);
			this.mainMenuItemNew.Text = "&Новый";
			this.mainMenuItemNew.Click += new System.EventHandler(this.mainMenuItemNew_Click);
			// 
			// mainMenuItemOpen
			// 
			this.mainMenuItemOpen.Name = "mainMenuItemOpen";
			this.mainMenuItemOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.mainMenuItemOpen.Size = new System.Drawing.Size(225, 22);
			this.mainMenuItemOpen.Text = "&Открыть";
			this.mainMenuItemOpen.Click += new System.EventHandler(this.mainMenuItemOpen_Click);
			// 
			// mainMenuSeparator1
			// 
			this.mainMenuSeparator1.Name = "mainMenuSeparator1";
			this.mainMenuSeparator1.Size = new System.Drawing.Size(222, 6);
			// 
			// mainMenuItemSave
			// 
			this.mainMenuItemSave.Name = "mainMenuItemSave";
			this.mainMenuItemSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.mainMenuItemSave.Size = new System.Drawing.Size(225, 22);
			this.mainMenuItemSave.Text = "&Сохранить";
			this.mainMenuItemSave.Click += new System.EventHandler(this.mainMenuItemSave_Click);
			// 
			// mainMenuItemSaveAs
			// 
			this.mainMenuItemSaveAs.Name = "mainMenuItemSaveAs";
			this.mainMenuItemSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
			this.mainMenuItemSaveAs.Size = new System.Drawing.Size(225, 22);
			this.mainMenuItemSaveAs.Text = "Сохранить &как";
			this.mainMenuItemSaveAs.Click += new System.EventHandler(this.mainMenuItemSaveAs_Click);
			// 
			// mainMenuSeparator2
			// 
			this.mainMenuSeparator2.Name = "mainMenuSeparator2";
			this.mainMenuSeparator2.Size = new System.Drawing.Size(222, 6);
			// 
			// mainMenuItemMerge
			// 
			this.mainMenuItemMerge.Name = "mainMenuItemMerge";
			this.mainMenuItemMerge.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
			this.mainMenuItemMerge.Size = new System.Drawing.Size(225, 22);
			this.mainMenuItemMerge.Text = "Импорт";
			this.mainMenuItemMerge.Click += new System.EventHandler(this.mainMenuItemMerge_Click);
			// 
			// mainMenuSeparator5
			// 
			this.mainMenuSeparator5.Name = "mainMenuSeparator5";
			this.mainMenuSeparator5.Size = new System.Drawing.Size(222, 6);
			// 
			// mainMenuItemExit
			// 
			this.mainMenuItemExit.Name = "mainMenuItemExit";
			this.mainMenuItemExit.Size = new System.Drawing.Size(225, 22);
			this.mainMenuItemExit.Text = "В&ыход";
			this.mainMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
			// 
			// mainMenuItemEdit
			// 
			this.mainMenuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuItemUndo,
            this.mainMenuItemRedo,
            this.mainMenuSeparator4,
            this.mainMenuItemCut,
            this.mainMenuItemCopy,
            this.mainMenuItemDelete,
            this.mainMenuItemClean,
            this.mainMenuItemDeleteMask});
			this.mainMenuItemEdit.Name = "mainMenuItemEdit";
			this.mainMenuItemEdit.Size = new System.Drawing.Size(59, 20);
			this.mainMenuItemEdit.Text = "&Правка";
			// 
			// mainMenuItemUndo
			// 
			this.mainMenuItemUndo.Name = "mainMenuItemUndo";
			this.mainMenuItemUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
			this.mainMenuItemUndo.Size = new System.Drawing.Size(220, 22);
			this.mainMenuItemUndo.Text = "&Отменить";
			this.mainMenuItemUndo.Click += new System.EventHandler(this.mainMenuItemUndo_Click);
			// 
			// mainMenuItemRedo
			// 
			this.mainMenuItemRedo.Name = "mainMenuItemRedo";
			this.mainMenuItemRedo.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.Z)));
			this.mainMenuItemRedo.Size = new System.Drawing.Size(220, 22);
			this.mainMenuItemRedo.Text = "Пов&торить";
			this.mainMenuItemRedo.Click += new System.EventHandler(this.mainMenuItemRedo_Click);
			// 
			// mainMenuSeparator4
			// 
			this.mainMenuSeparator4.Name = "mainMenuSeparator4";
			this.mainMenuSeparator4.Size = new System.Drawing.Size(217, 6);
			// 
			// mainMenuItemDelete
			// 
			this.mainMenuItemDelete.Name = "mainMenuItemDelete";
			this.mainMenuItemDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
			this.mainMenuItemDelete.Size = new System.Drawing.Size(220, 22);
			this.mainMenuItemDelete.Text = "&Удалить";
			this.mainMenuItemDelete.Click += new System.EventHandler(this.mainMenuItemDelete_Click);
			// 
			// mainMenuItemClean
			// 
			this.mainMenuItemClean.Name = "mainMenuItemClean";
			this.mainMenuItemClean.Size = new System.Drawing.Size(220, 22);
			this.mainMenuItemClean.Text = "Удалить &всё";
			this.mainMenuItemClean.Click += new System.EventHandler(this.mainMenuItemClean_Click);
			// 
			// mainMenuItemDeleteMask
			// 
			this.mainMenuItemDeleteMask.Name = "mainMenuItemDeleteMask";
			this.mainMenuItemDeleteMask.Size = new System.Drawing.Size(220, 22);
			this.mainMenuItemDeleteMask.Text = "Удалить от&фильтрованное";
			this.mainMenuItemDeleteMask.Click += new System.EventHandler(this.mainMenuItemDeleteMask_Click);
			// 
			// mainMenuItemHelp
			// 
			this.mainMenuItemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuItemManual,
            this.mainMenuSeparator3,
            this.mainMenuItemAbout});
			this.mainMenuItemHelp.Name = "mainMenuItemHelp";
			this.mainMenuItemHelp.Size = new System.Drawing.Size(65, 20);
			this.mainMenuItemHelp.Text = "&Справка";
			// 
			// mainMenuItemManual
			// 
			this.mainMenuItemManual.Name = "mainMenuItemManual";
			this.mainMenuItemManual.Size = new System.Drawing.Size(149, 22);
			this.mainMenuItemManual.Text = "&Руководство";
			this.mainMenuItemManual.Click += new System.EventHandler(this.mainMenuItemManual_Click);
			// 
			// mainMenuSeparator3
			// 
			this.mainMenuSeparator3.Name = "mainMenuSeparator3";
			this.mainMenuSeparator3.Size = new System.Drawing.Size(146, 6);
			// 
			// mainMenuItemAbout
			// 
			this.mainMenuItemAbout.Name = "mainMenuItemAbout";
			this.mainMenuItemAbout.Size = new System.Drawing.Size(149, 22);
			this.mainMenuItemAbout.Text = "&О программе";
			this.mainMenuItemAbout.Click += new System.EventHandler(this.mainMenuItemAbout_Click);
			// 
			// labelFilter
			// 
			this.labelFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelFilter.AutoSize = true;
			this.labelFilter.Location = new System.Drawing.Point(18, 254);
			this.labelFilter.Name = "labelFilter";
			this.labelFilter.Size = new System.Drawing.Size(51, 15);
			this.labelFilter.TabIndex = 6;
			this.labelFilter.Text = "Фильтр:";
			// 
			// checkBoxSort
			// 
			this.checkBoxSort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxSort.AutoSize = true;
			this.checkBoxSort.Location = new System.Drawing.Point(283, 31);
			this.checkBoxSort.Name = "checkBoxSort";
			this.checkBoxSort.Size = new System.Drawing.Size(92, 19);
			this.checkBoxSort.TabIndex = 4;
			this.checkBoxSort.Text = "Сортировка";
			this.checkBoxSort.UseVisualStyleBackColor = true;
			// 
			// buttonAdd
			// 
			this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonAdd.Location = new System.Drawing.Point(201, 27);
			this.buttonAdd.Name = "buttonAdd";
			this.buttonAdd.Size = new System.Drawing.Size(73, 25);
			this.buttonAdd.TabIndex = 3;
			this.buttonAdd.Text = "Добавить";
			this.buttonAdd.UseVisualStyleBackColor = true;
			this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// textBoxElement
			// 
			this.textBoxElement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.errorProvider.SetIconPadding(this.textBoxElement, -20);
			this.textBoxElement.Location = new System.Drawing.Point(75, 28);
			this.textBoxElement.MaxLength = 10;
			this.textBoxElement.Name = "textBoxElement";
			this.textBoxElement.Size = new System.Drawing.Size(120, 23);
			this.textBoxElement.TabIndex = 2;
			this.textBoxElement.TextChanged += new System.EventHandler(this.textBoxElement_TextChanged);
			// 
			// labelElement
			// 
			this.labelElement.AutoSize = true;
			this.labelElement.Location = new System.Drawing.Point(12, 31);
			this.labelElement.Name = "labelElement";
			this.labelElement.Size = new System.Drawing.Size(57, 15);
			this.labelElement.TabIndex = 1;
			this.labelElement.Text = "Элемент:";
			// 
			// errorProvider
			// 
			this.errorProvider.ContainerControl = this;
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.DefaultExt = "lbx2";
			this.saveFileDialog.Filter = "Файлы ListBoxer 2 (*.lbx2)|*.lbx2|Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*" +
    ".*";
			this.saveFileDialog.FilterIndex = 0;
			// 
			// openFileDialog
			// 
			this.openFileDialog.DefaultExt = "lbx2";
			this.openFileDialog.Filter = "Файлы ListBoxer 2 (*.lbx2)|*.lbx2|Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*" +
    ".*";
			this.openFileDialog.FilterIndex = 0;
			// 
			// statusBar
			// 
			this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabelElementCount,
            this.statusBarLabelFilterCount,
            this.statusBarLabelModified});
			this.statusBar.Location = new System.Drawing.Point(0, 282);
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(389, 24);
			this.statusBar.TabIndex = 8;
			this.statusBar.Text = "statusStrip1";
			// 
			// statusBarLabelElementCount
			// 
			this.statusBarLabelElementCount.AutoSize = false;
			this.statusBarLabelElementCount.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.statusBarLabelElementCount.Name = "statusBarLabelElementCount";
			this.statusBarLabelElementCount.Size = new System.Drawing.Size(170, 19);
			this.statusBarLabelElementCount.Text = "ElementCount";
			// 
			// statusBarLabelFilterCount
			// 
			this.statusBarLabelFilterCount.AutoSize = false;
			this.statusBarLabelFilterCount.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.statusBarLabelFilterCount.Name = "statusBarLabelFilterCount";
			this.statusBarLabelFilterCount.Size = new System.Drawing.Size(130, 19);
			this.statusBarLabelFilterCount.Text = "FilterCount";
			// 
			// statusBarLabelModified
			// 
			this.statusBarLabelModified.AutoSize = false;
			this.statusBarLabelModified.Name = "statusBarLabelModified";
			this.statusBarLabelModified.Size = new System.Drawing.Size(70, 19);
			this.statusBarLabelModified.Text = "Modified";
			// 
			// mainMenuItemCopy
			// 
			this.mainMenuItemCopy.Name = "mainMenuItemCopy";
			this.mainMenuItemCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
			this.mainMenuItemCopy.Size = new System.Drawing.Size(220, 22);
			this.mainMenuItemCopy.Text = "Копировать";
			this.mainMenuItemCopy.Click += new System.EventHandler(this.mainMenuItemCopy_Click);
			// 
			// mainMenuItemCut
			// 
			this.mainMenuItemCut.Name = "mainMenuItemCut";
			this.mainMenuItemCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
			this.mainMenuItemCut.Size = new System.Drawing.Size(220, 22);
			this.mainMenuItemCut.Text = "Вырезать";
			this.mainMenuItemCut.Click += new System.EventHandler(this.mainMenuItemCut_Click);
			// 
			// MainWindow
			// 
			this.AcceptButton = this.buttonAdd;
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.ClientSize = new System.Drawing.Size(389, 306);
			this.Controls.Add(this.statusBar);
			this.Controls.Add(this.labelElement);
			this.Controls.Add(this.checkBoxSort);
			this.Controls.Add(this.buttonAdd);
			this.Controls.Add(this.textBoxElement);
			this.Controls.Add(this.labelFilter);
			this.Controls.Add(this.textBoxFilter);
			this.Controls.Add(this.listBoxDocument);
			this.Controls.Add(this.mainMenu);
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MainMenuStrip = this.mainMenu;
			this.MinimumSize = new System.Drawing.Size(405, 230);
			this.Name = "MainWindow";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ListBoxer 2";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainWindow_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainWindow_DragEnter);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainWindow_KeyPress);
			this.mainMenu.ResumeLayout(false);
			this.mainMenu.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			this.statusBar.ResumeLayout(false);
			this.statusBar.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private ListBox listBoxDocument;
		private TextBox textBoxFilter;
		private MenuStrip mainMenu;
		private ToolStripMenuItem mainMenuItemFile;
		private ToolStripMenuItem mainMenuItemExit;
		private ToolStripMenuItem mainMenuItemNew;
		private ToolStripMenuItem mainMenuItemOpen;
		private ToolStripSeparator mainMenuSeparator1;
		private ToolStripMenuItem mainMenuItemSave;
		private ToolStripMenuItem mainMenuItemSaveAs;
		private ToolStripSeparator mainMenuSeparator2;
		private ToolStripMenuItem mainMenuItemEdit;
		private ToolStripMenuItem mainMenuItemUndo;
		private ToolStripMenuItem mainMenuItemRedo;
		private ToolStripMenuItem mainMenuItemHelp;
		private ToolStripMenuItem mainMenuItemManual;
		private ToolStripSeparator mainMenuSeparator3;
		private ToolStripMenuItem mainMenuItemAbout;
		private ToolStripSeparator mainMenuSeparator4;
		private ToolStripMenuItem mainMenuItemDelete;
		private Label labelFilter;
		private CheckBox checkBoxSort;
		private Button buttonAdd;
		private TextBox textBoxElement;
		private Label labelElement;
		private ErrorProvider errorProvider;
		private SaveFileDialog saveFileDialog;
		private OpenFileDialog openFileDialog;
		private ToolStripMenuItem mainMenuItemMerge;
		private ToolStripSeparator mainMenuSeparator5;
		private StatusStrip statusBar;
		private ToolStripStatusLabel statusBarLabelElementCount;
		private ToolStripStatusLabel statusBarLabelFilterCount;
		private ToolStripStatusLabel statusBarLabelModified;
		private ToolStripMenuItem mainMenuItemClean;
		private ToolStripMenuItem mainMenuItemDeleteMask;
		private ToolStripMenuItem mainMenuItemCut;
		private ToolStripMenuItem mainMenuItemCopy;
	}
}

