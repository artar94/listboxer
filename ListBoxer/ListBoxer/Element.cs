﻿using System;
using System.Linq;

namespace ListBoxer
{
	public class Element: IComparable<Element>
	{
		public string Value { get; }

		public Element(string line)
		{
			if (line.Length < 1 || line.Length > 10)
				throw new ArgumentOutOfRangeException(nameof(line), @"The string has an invalid length");
			if (!line.All(char.IsLetterOrDigit))
				throw new ArgumentException(@"The string contains an invalid character", nameof(line));
			Value = line;
		}

		public int CompareTo(Element other)
		{
			return string.Compare(Value, other.Value, StringComparison.OrdinalIgnoreCase);
		}

		public override string ToString()
		{
			return Value;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((Element)obj);
		}

		protected bool Equals(Element other)
		{
			return string.Equals(Value, other.Value);
		}

		public override int GetHashCode()
		{
			return Value?.GetHashCode() ?? 0;
		}

		public static bool operator ==(Element left, Element right)
		{
			if (ReferenceEquals(left, right))
				return true;
			
			if (((object)left == null) || ((object)right == null))
				return false;

			return left.Value == right.Value;
		}

		public static bool operator !=(Element left, Element right)
		{
			return !(left == right);
		}

		public static bool operator >(Element left, Element right)
		{
			return right != null && left != null && (left.CompareTo(right) > 0);
		}

		public static bool operator <(Element left, Element right)
		{
			return right != null && left != null && (left.CompareTo(right) < 0);
		}

		public static bool operator >=(Element left, Element right)
		{
			return right != null && left != null && (left.CompareTo(right) >= 0);
		}

		public static bool operator <=(Element left, Element right)
		{
			return right != null && left != null && (left.CompareTo(right) <= 0);
		}
	}
}