﻿using System.Collections.Generic;

namespace ListBoxer
{
	public delegate void DocumentChangeEvent(object sender, int index, Element element);
	public delegate void DocumentSortEvent(object sender, bool sorted);
	public delegate void DocumentSavedEvent(object sender, string fileName);

	public interface IVersionedCollection : ICollection<Element>
	{
		History History { get; }
		Element this[int index] { get; }

		void AddRange(ICollection<Element> collection);
		void RemoveAt(int index);
	}

	public interface IDocument: IVersionedCollection
	{
		bool AutoSort { get; set; }
		string FileName { get; }

		void SaveToFile(string fileName);

		event DocumentChangeEvent Inserted;
		event DocumentChangeEvent Removed;
		event DocumentSortEvent Sorted;
		event DocumentSavedEvent Saved;
	}

	public interface IDirectDocument : IDocument
	{
		void DirectInsert(int index, Element element);
		void DirectRemove(int index);
		void DirectSort(bool sorted);
	}
}