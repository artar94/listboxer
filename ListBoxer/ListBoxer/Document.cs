﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ListBoxer.HistoryItems;

namespace ListBoxer
{
	public class Document: IDirectDocument
	{
		private readonly List<Element> _elements = new List<Element>();
		private bool _sort;

		public History History { get; }

		public bool AutoSort
		{
			get
			{
				return _sort;
			}
			set
			{
				if (_sort == value) return;

				if (value)
					History.Add(new SortHistoryItem());
				else
					History.Add(new UnsortHistoryItem());

				_sort = value;
			}
		}

		public string FileName { get; private set; } = "";
		public int Count => _elements.Count;
		public bool IsReadOnly => false;

		public event DocumentChangeEvent Inserted;
		public event DocumentChangeEvent Removed;
		public event DocumentSortEvent Sorted;
		public event DocumentSavedEvent Saved;

		public Document()
		{
			History = new History(this);
		}

		public Document(string fileName)
		{
			var file = new StreamReader(fileName, Encoding.UTF8);
			try
			{
				while (!file.EndOfStream)
				{
					_elements.Add(new Element(file.ReadLine()));
				}
			}
			finally
			{
				file.Close();
			}
			FileName = fileName;

			History = new History(this);
		}

		public void SaveToFile(string fileName)
		{
			var file = new StreamWriter(fileName, false, Encoding.UTF8);
			try
			{
				_elements.ForEach(file.WriteLine);
			}
			finally
			{
				file.Close();
			}
			FileName = fileName;
			History.IsModified = false;
			Saved?.Invoke(this, fileName);
		}

		public Element this[int index] => _elements[index];

		public IEnumerator<Element> GetEnumerator()
		{
			return _elements.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Add(Element item)
		{
			History.Add(new AddHistoryItem(item));
		}

		public void AddRange(ICollection<Element> collection)
		{
			History.Add(new MergeHistoryItem(collection));
		}

		public void Clear()
		{
			var cleanHistoryItem = new CompositeHistoryItem();
			for (var i = _elements.Count - 1; i >= 0; i--)
				cleanHistoryItem.Add(new RemoveHistoryItem(i));
			History.Add(cleanHistoryItem);
		}

		public bool Contains(Element item)
		{
			return _elements.Contains(item);
		}

		public void CopyTo(Element[] array, int arrayIndex)
		{
			_elements.CopyTo(array, arrayIndex);
		}

		public bool Remove(Element item)
		{
			var index = AutoSort ? _elements.BinarySearch(item) : _elements.IndexOf(item);
			if (index < 0) return false;

			History.Add(new RemoveHistoryItem(index));
			return true;
		}

		public void RemoveAt(int index)
		{
			History.Add(new RemoveHistoryItem(index));
		}

		public void DirectInsert(int index, Element element)
		{
			_elements.Insert(index, element);
			Inserted?.Invoke(this, index, element);
		}

		public void DirectRemove(int index)
		{
			var element = _elements[index];
			_elements.RemoveAt(index);
			Removed?.Invoke(this, index, element);
		}

		public void DirectSort(bool sorted)
		{
			_sort = sorted;
			Sorted?.Invoke(this, _sort);
		}
	}
}
