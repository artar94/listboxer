﻿namespace ListBoxer.HistoryItems
{
	public class UnsortHistoryItem: HistoryItem
	{
		public override void Undo(IDirectDocument document)
		{
			if (document.AutoSort)
				return;

			document.DirectSort(true);
		}

		public override void Redo(IDirectDocument document)
		{
			if (!document.AutoSort)
				return;

			document.DirectSort(false);
		}
	}
}