﻿using System.Linq;

namespace ListBoxer.HistoryItems
{
	public class AddHistoryItem: HistoryItem
	{
		private readonly Element _element;
		private int _index;

		public AddHistoryItem(Element element)
		{
			_element = element;
		}

		public override void Undo(IDirectDocument document)
		{
			document.DirectRemove(_index);
		}

		public override void Redo(IDirectDocument document)
		{
			var index = document.AutoSort ? document.ToList().BinarySearch(_element) : document.Count;

			_index = index < 0 ? ~index : index;

			document.DirectInsert(_index, _element);
		}
	}
}