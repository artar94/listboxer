﻿using System.Collections.Generic;
using System.Linq;

namespace ListBoxer.HistoryItems
{
	public class MergeHistoryItem: HistoryItem
	{
		private readonly ICollection<Element> _mergeElements;
		private List<Element> _unmergedElements;

		public MergeHistoryItem(ICollection<Element> mergeElements)
		{
			_mergeElements = mergeElements;
		} 

		public override void Undo(IDirectDocument document)
		{
			if (_mergeElements.Count == 0)
				return;

			for (var i = 0; i < _unmergedElements.Count; i++)
				if (_unmergedElements[i] != document[i])
				{
					document.DirectRemove(i);
					document.DirectInsert(i, _unmergedElements[i]);
				}

			for (var i = document.Count - 1; i >= _unmergedElements.Count; i--)
				document.DirectRemove(i);
		}

		public override void Redo(IDirectDocument document)
		{
			if (_mergeElements.Count == 0)
				return;

			_unmergedElements = document.ToList();

			var elements = document.ToList();
			elements.AddRange(_mergeElements);

			if (document.AutoSort)
				elements.Sort();

			for (var i = 0; i < document.Count; i++)
				if (elements[i] != document[i])
				{
					document.DirectRemove(i);
					document.DirectInsert(i, elements[i]);
				}

			for (var i = document.Count; i < elements.Count; i++)
				document.DirectInsert(i, elements[i]);
		}
	}
}