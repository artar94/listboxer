﻿using System.Collections.Generic;
using System.Linq;

namespace ListBoxer.HistoryItems
{
	public class SortHistoryItem : HistoryItem
	{
		private List<Element> _unsortedElements;

		public override void Undo(IDirectDocument document)
		{
			if (!document.AutoSort)
				return;

			for (var i = 0; i < document.Count; i++)
				if (_unsortedElements[i] != document[i])
				{
					document.DirectRemove(i);
					document.DirectInsert(i, _unsortedElements[i]);
				}

			document.DirectSort(false);
		}

		public override void Redo(IDirectDocument document)
		{
			if (document.AutoSort)
				return;

			_unsortedElements = document.ToList();
			
			var elements = document.ToList();
			elements.Sort();

			for (var i = 0; i < document.Count; i++)
				if (elements[i] != document[i])
				{
					document.DirectRemove(i);
					document.DirectInsert(i, elements[i]);
				}

			document.DirectSort(true);
		}
	}
}