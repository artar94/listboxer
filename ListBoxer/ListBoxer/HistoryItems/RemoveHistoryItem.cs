﻿namespace ListBoxer.HistoryItems
{
	public class RemoveHistoryItem : HistoryItem
	{
		private readonly int _index;
		private Element _element;

		public RemoveHistoryItem(int index)
		{
			_index = index;
		}

		public override void Undo(IDirectDocument document)
		{
			document.DirectInsert(_index, _element);
		}

		public override void Redo(IDirectDocument document)
		{
			_element = document[_index];
			document.DirectRemove(_index);
		}
	}
}