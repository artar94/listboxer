﻿using System;
using System.Collections.Generic;
using System.Linq;
using ListBoxer;
using ListBoxer.Filtering;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TddTesting
{
	[TestClass]
	public class PcreFilterUnitTests
	{
		private IDocument _document;
		internal volatile IFilter VolatileFilter;

		public struct FilterEventArgs
		{
			public object Sender;
			public int Index;
			public IFilterElement Element;

			public FilterEventArgs(object sender, int index, IFilterElement element)
			{
				Sender = sender;
				Index = index;
				Element = element;
			}
		}

		[TestInitialize]
		public void Initialize()
		{
			_document = new Document();
		}

		[TestMethod, Description("Отключенный фильтр должен сожержать все элементы документа")]
		public void DisabledFilterTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("09"),
				new Element("qwer"),
				new Element("09qwerйцук"),
				new Element("QWERqwer"),
				new Element("ЙЦУКйцук"),
			};
			var expectedElements = elements.Select((e, i) => new PcreFilterElement(e, i)).ToList();

			_document.AddRange(elements);

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Отключенный фильтр должен вызвать событие вставки на добавление каждого элемента в документ")]
		public void DisabledFilterInsertedEventTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("element0"),
				new Element("element1"),
				new Element("element2"),
			};
			var expectedEvents = elements.Select((e, i) => new FilterEventArgs(filter, i, new PcreFilterElement(e, i))).ToList();

			var actualEvents = new List<FilterEventArgs>();
			filter.Inserted += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));

			_document.AddRange(elements);

			CollectionAssert.AreEqual(expectedEvents, actualEvents);
		}

		[TestMethod, Description("Отключенный фильтр должен вызвать событие удаления на удалении каждого элемента из документа")]
		public void DisabledFilterRemoveEventTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("element0"),
				new Element("element1"),
				new Element("element2"),
			};
			var expectedEvents = new List<FilterEventArgs>();
			for (var i = elements.Count - 1; i >= 0; i--)
				expectedEvents.Add(new FilterEventArgs(filter, i, new PcreFilterElement(elements[i], i)));

			_document.AddRange(elements);

			var actualEvents = new List<FilterEventArgs>();
			filter.Removed += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));
			for (var i = _document.Count - 1; i >= 0; i--)
				_document.RemoveAt(i);

			CollectionAssert.AreEqual(expectedEvents, actualEvents);
		}

		[TestMethod, Description("Включенный фильтр должен сожержать только элементы, подходящие под шаблон")]
		public void EnabledFilterTest()
		{
			var filter = new PcreFilter(_document, "^[0-9]+$");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
				new Element("QWERqwer"),
				new Element("ЙЦУКйцук"),
			};
			var expectedElements = new List<PcreFilterElement>
			{
				new PcreFilterElement(elements[0], 0),
				new PcreFilterElement(elements[2], 2),
			};

			_document.AddRange(elements);

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Включенный фильтр должен вызвать событие вставки только для элементов, подходящии под шаблон")]
		public void EnabledFilterInsertedEventTest()
		{
			var filter = new PcreFilter(_document, "^[0-9]+$");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
			};
			var expectedEvents = new List<FilterEventArgs>
			{
				new FilterEventArgs(filter, 0, new PcreFilterElement(elements[0], 0)),
				new FilterEventArgs(filter, 1, new PcreFilterElement(elements[2], 2)),
			};

			var actualEvents = new List<FilterEventArgs>();
			filter.Inserted += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));

			_document.AddRange(elements);

			CollectionAssert.AreEqual(expectedEvents, actualEvents);
		}

		[TestMethod, Description("Включенный фильтр должен вызвать события удаления только для элементов, подходящии под шаблон")]
		public void EnabledFilterRemovedEventTest()
		{
			var filter = new PcreFilter(_document, "^[0-9]+$");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
			};
			var expectedEvents = new List<FilterEventArgs>
			{
				new FilterEventArgs(filter, 1, new PcreFilterElement(elements[2], 2)),
				new FilterEventArgs(filter, 0, new PcreFilterElement(elements[0], 0)),
			};

			_document.AddRange(elements);

			var actualEvents = new List<FilterEventArgs>();
			filter.Removed += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));

			for (var i = _document.Count - 1; i >= 0; i--)
				_document.RemoveAt(i);

			CollectionAssert.AreEqual(expectedEvents, actualEvents);
		}

		[TestMethod, Description("Включение фильтра должно оставить только элементы, подходящии под шаблон")]
		public void EnablingFilterTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("asd"),
				new Element("09"),
				new Element("09qwerйцук"),
			};
			var expectedElements = new List<PcreFilterElement>
			{
				new PcreFilterElement(elements[0], 0),
				new PcreFilterElement(elements[3], 3),
			};

			_document.AddRange(elements);

			filter.Pattern = "^[0-9]+$";

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Включение фильтра должено вызвать события удаления элементов, не подходящих под шаблон")]
		public void EnablingFilterRemoveEventTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
			};
			var expectedEvents = new List<FilterEventArgs>
			{
				new FilterEventArgs(filter, 1, new PcreFilterElement(elements[1], 1)),
				new FilterEventArgs(filter, 2, new PcreFilterElement(elements[3], 3)),
			};

			_document.AddRange(elements);

			var actualEvents = new List<FilterEventArgs>();
			filter.Removed += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));
			filter.Pattern = "^[0-9]+$";

			CollectionAssert.AreEqual(expectedEvents, actualEvents);
		}

		[TestMethod, Description("Отключение фильтра должно отобразить все элементы документа")]
		public void DisablingFilterTest()
		{
			var filter = new PcreFilter(_document, "^[0-9]+$");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
			};
			var expectedElements = elements.Select((e, i) => new PcreFilterElement(e, i)).ToList();

			_document.AddRange(elements);

			filter.Pattern = ".+";

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Отключение фильтра должено вызвать события добавления элементов, отсутствующих в фильтре")]
		public void DisablingFilterInsertEventTest()
		{
			var filter = new PcreFilter(_document, "^[0-9]+$");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("qwer"),
				new Element("09"),
				new Element("09qwerйцук"),
			};
			var expectedEvents = new List<FilterEventArgs>
			{
				new FilterEventArgs(filter, 1, new PcreFilterElement(elements[1], 1)),
				new FilterEventArgs(filter, 3, new PcreFilterElement(elements[3], 3)),
			};

			_document.AddRange(elements);

			var actualEvents = new List<FilterEventArgs>();
			filter.Inserted += (sender, index, element) => actualEvents.Add(new FilterEventArgs(sender, index, element));
			filter.Pattern = ".+";

			CollectionAssert.AreEqual(expectedEvents, actualEvents);
		}

		[TestMethod, Description("Документ в конструкторе не может быть null")]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ConstructorNullDocumentTest()
		{
			VolatileFilter = new PcreFilter(null, ".+");
		}

		[TestMethod, Description("Шаблон в конструкторе не может быть null")]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ConstructorNullFilterTest()
		{
			VolatileFilter = new PcreFilter(_document, null);
		}

		[TestMethod, Description("Шаблон не может быть null")]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullFilterTest()
		{
			VolatileFilter = new PcreFilter(_document, ".+") {Pattern = null};
		}

		[TestMethod, Description("Пустой шаблон фильтра должен сожержать все элементы документа")]
		public void EmptyFilterTest()
		{
			var filter = new PcreFilter(_document, "");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("09"),
				new Element("qwer"),
				new Element("09qwerйцук"),
				new Element("QWERqwer"),
				new Element("ЙЦУКйцук"),
			};
			var expectedElements = elements.Select((e, i) => new PcreFilterElement(e, i)).ToList();

			_document.AddRange(elements);

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Элементы должны инициализироваться в конструкторе")]
		public void ElementsInitTest()
		{
			var elements = new List<Element>
			{
				new Element("0"),
				new Element("09"),
				new Element("qwer"),
				new Element("09qwerйцук"),
			};
			var expectedElements = elements.Select((e, i) => new PcreFilterElement(e, i)).ToList();

			_document.AddRange(elements);

			var filter = new PcreFilter(_document, ".+");

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Свойство Pattern должно инициализироваться в конструкторе")]
		public void PatternInitTest()
		{
			var filter = new PcreFilter(_document, ".+");
			Assert.AreEqual(".+", filter.Pattern);
		}

		[TestMethod, Description("Добавление элемента в середину списка, попавшего в фильтр, должно приводить к обновлению индексов элементов фильтра")]
		public void AddCenterFilteredTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("1"),
				new Element("4"),
				new Element("3"),
				new Element("2"),
				new Element("0"),
				new Element("1"),
			};
			var expectedElements = new List<PcreFilterElement>
			{
				new PcreFilterElement(elements[4], 0),
				new PcreFilterElement(elements[0], 1),
				new PcreFilterElement(elements[5], 2),
				new PcreFilterElement(elements[3], 3),
				new PcreFilterElement(elements[2], 4),
				new PcreFilterElement(elements[1], 5),
			};

			_document.AutoSort = true;
			_document.AddRange(elements);

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Удаление элемента из середины списка, попавшего в фильтр, должно приводить к обновлению индексов элементов фильтра")]
		public void RemoveCenterFilteredTest()
		{
			var filter = new PcreFilter(_document, ".+");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("09"),
				new Element("qwer"),
				new Element("09qwerйцук"),
			};
			var expectedElements = new List<PcreFilterElement>
			{
				new PcreFilterElement(elements[0], 0),
				new PcreFilterElement(elements[2], 1),
				new PcreFilterElement(elements[3], 2),
			};

			_document.AddRange(elements);
			_document.RemoveAt(1);

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}

		[TestMethod, Description("Удаление элемента из начала списка, не попавшего в фильтр, должно приводить к обновлению индексов элементов фильтра")]
		public void RemoveFirstNotFilteredTest()
		{
			var filter = new PcreFilter(_document, "[q]");

			var elements = new List<Element>
			{
				new Element("0"),
				new Element("09"),
				new Element("qwer"),
				new Element("09qwerйцук"),
			};
			var expectedElements = new List<PcreFilterElement>
			{
				new PcreFilterElement(elements[2], 1),
				new PcreFilterElement(elements[3], 2),
			};

			_document.AddRange(elements);
			_document.RemoveAt(0);

			CollectionAssert.AreEqual(expectedElements, filter.ToList());
		}
	}
}
