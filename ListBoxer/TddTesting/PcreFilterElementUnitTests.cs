﻿using System;
using ListBoxer;
using ListBoxer.Filtering;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TddTesting
{
	[TestClass]
	public class PcreFilterElementUnitTests
	{
		private Element _element;
		internal volatile IFilterElement VolatileFilterElement;

		[TestInitialize]
		public void Initialize()
		{
			_element = new Element("element0");
		}

		[TestMethod, Description("Свойство DocumentIndex должно инициализироваться в конструкторе")]
		public void DocumentIndexInitTest()
		{
			var filterElement = new PcreFilterElement(_element, 1);
			Assert.AreEqual(1, filterElement.DocumentIndex);
		}

		[TestMethod, Description("Свойство Element должно инициализироваться в конструкторе")]
		public void ElementInitTest()
		{
			var filterElement = new PcreFilterElement(_element, 1);
			Assert.AreEqual(_element, filterElement.Element);
		}

		[TestMethod, Description("Свойство DocumentIndex должно изменять значение")]
		public void DocumentIndexSetTest()
		{
			var filterElement = new PcreFilterElement(_element, 0)
			{
				DocumentIndex = 1
			};
			Assert.AreEqual(1, filterElement.DocumentIndex);
		}

		[TestMethod, Description("Элемент в конструкторе не может быть null")]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ConstructorNullElementTest()
		{
			VolatileFilterElement = new PcreFilterElement(null, 0);
		}
	}
}
