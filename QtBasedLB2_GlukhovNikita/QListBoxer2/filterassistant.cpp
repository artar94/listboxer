#include "filterassistant.h"
#include <QRegExp>
#include <QRegExpValidator>

FilterAssistant::FilterAssistant()
{

}

bool FilterAssistant::filterRusValidator(QString str)
{
    int pos = 0;

    QRegExp rx("[абвгдеёжзийклмнопрстуфхцчшщъыьэюя0-9 ]+");
    rx.setCaseSensitivity(Qt::CaseInsensitive);

    QRegExpValidator v(rx,0);

    if(v.validate(str,pos)==QValidator::Acceptable)
        return true;
    else
        return false;
}

bool FilterAssistant::filterEngValidator(QString str)
{
    int pos = 0;
    QRegExp rx("[a-z0-9 ]+");
    rx.setCaseSensitivity(Qt::CaseInsensitive);

    QRegExpValidator v(rx,0);

    if(v.validate(str,pos)==QValidator::Acceptable)
        return true;
    else
        return false;
}

bool FilterAssistant::filterRusEngPunctValidator(QString str)
{
    int pos = 0;
    QRegExp rx("[абвгдеёжзийклмнопрстуфхцчшщъыьэюяa-z0-9.,:;! ]+");
    rx.setCaseSensitivity(Qt::CaseInsensitive);

    QRegExpValidator v(rx,0);

    if(v.validate(str,pos)==QValidator::Acceptable)
        return true;
    else
        return false;
}
