#ifndef FILTERASSISTANT_H
#define FILTERASSISTANT_H
#include <QString>


class FilterAssistant
{
private:
    FilterAssistant();

public:
    static bool filterRusValidator(QString);
    static bool filterEngValidator(QString);
    static bool filterRusEngPunctValidator(QString);

};

#endif // FILTERASSISTANT_H
