#-------------------------------------------------
#
# Project created by QtCreator 2016-04-13T17:41:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ListBoxer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    storage.cpp \
    sortassistant.cpp \
    filterassistant.cpp \
    finddialog.cpp

HEADERS  += mainwindow.h \
    listboxerdefs.h \
    storage.h \
    sortassistant.h \
    filterassistant.h \
    finddialog.h

FORMS    += mainwindow.ui

RESOURCES += \
    listboxer.qrc

RC_FILE = listboxer.rc
