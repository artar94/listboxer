#include "finddialog.h"


FindDialog::FindDialog(QWidget* parent):QDialog(parent,Qt::WindowTitleHint)
{
    leSearchMask = new QLineEdit;
    rbUp = new QRadioButton("Вверх");
    rbDown = new QRadioButton("Вниз");

    QPushButton* btnFind = new QPushButton("Найти");
    QPushButton* btnClose = new QPushButton("Закрыть");

    rbDown->setChecked(true);

    connect(btnFind, SIGNAL(clicked()), SLOT(accept()));
    connect(btnClose, SIGNAL(clicked()), SLOT(reject()));

    QGridLayout* ptopLayout = new QGridLayout;

    ptopLayout->addWidget(leSearchMask,0,0,1,0);
    ptopLayout->addWidget(rbUp,1,0,Qt::AlignHCenter);
    ptopLayout->addWidget(rbDown,1,1,Qt::AlignHCenter);
    ptopLayout->addWidget(btnFind,2,0);
    ptopLayout->addWidget(btnClose,2,1);

    setLayout(ptopLayout);

}

QString FindDialog::searchMask() const
{
    return leSearchMask->text();
}

bool FindDialog::isSearchDownward() const
{
    if(rbDown->isChecked())
        return true;
    else
        return false;
}
