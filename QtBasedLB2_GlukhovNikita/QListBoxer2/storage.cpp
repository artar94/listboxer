#include "storage.h"
#include "sortassistant.h"
#include <QFileDialog>
#include <QCollator>
#include <QTextStream>

bool Storage::performSorting()
{
    if(elementsList.size() >= 2 && sortType != ST_NONE)
    {
        if(sortType==ST_LEXIC && sortOrder==SO_DESC)
            SortAssistant::sortLexDesc(elementsList);
        else if (sortType==ST_LEXIC && sortOrder==SO_ASC)
            SortAssistant::sortLexAsc(elementsList);
        else if (sortType==ST_NATURAL && sortOrder==SO_DESC)
            SortAssistant::sortNatDesc(elementsList);
        else if (sortType==ST_NATURAL && sortOrder==SO_ASC)
            SortAssistant::sortNatAsc(elementsList);

        return true;
    }
    else
        return false;
}

void Storage::addAccordingToAscSort(const QString &str, std::function<bool (const QString &, const QString &)> less, std::function<bool (const QString &, const QString &)> ge)
{
    if(elementsList.size()==1)
    {
        if(less(elementsList.first(),str))
        {
            elementsList.append(str);
            indexOfLastAdded=1;
        }
        else
        {
            elementsList.prepend(str);
            indexOfLastAdded=0;
        }
    }
    else if(elementsList.size()==2)
    {
        if(less(elementsList.first(),str) && less(elementsList.last(),str))
        {
            elementsList.append(str);
            indexOfLastAdded=2;
        }
        else if(less(elementsList.first(),str) && ge(elementsList.last(),str))
        {
            elementsList.insert(1,str);
            indexOfLastAdded=1;
        }
        else
        {
            elementsList.prepend(str);
            indexOfLastAdded=0;
        }
    }
    else
    {
        if(less(elementsList.first(),str) && less(elementsList.last(),str))
        {
            elementsList.append(str);
            indexOfLastAdded=elementsList.size()-1;
        }
        else if(ge(elementsList.first(),str) && ge(elementsList.last(),str))
        {
            elementsList.prepend(str);
            indexOfLastAdded=0;
        }
        else
        {
            int a=0;
            int b=elementsList.size()-1;

            while((b-a-1) > 0)
            {
                int mid=(b-a)/2;

                if(less(elementsList.at(a),str) && ge(elementsList.at(a+mid),str))
                    b=a+mid;
                else
                    a=a+mid;
            }
            elementsList.insert(b,str);
            indexOfLastAdded=b;
        }
    }
}

void Storage::addAccordingToDescSort(const QString &str, std::function<bool (const QString &, const QString &)> ge, std::function<bool (const QString &, const QString &)> less)
{
    if(elementsList.size()==1)
    {
        if(ge(elementsList.first(),str))
        {
            elementsList.append(str);
            indexOfLastAdded=1;
        }
        else
        {
            elementsList.prepend(str);
            indexOfLastAdded=0;
        }
    }
    else if(elementsList.size()==2)
    {
        if(ge(elementsList.first(),str) && ge(elementsList.last(),str))
        {
            elementsList.append(str);
            indexOfLastAdded=2;
        }
        else if(ge(elementsList.first(),str) && less(elementsList.last(),str))
        {
            elementsList.insert(1,str);
            indexOfLastAdded=1;
        }
        else
        {
            elementsList.prepend(str);
            indexOfLastAdded=0;
        }
    }
    else
    {
        if(ge(elementsList.first(),str) && ge(elementsList.last(),str))
        {
            elementsList.append(str);
            indexOfLastAdded=elementsList.size()-1;
        }
        else if(less(elementsList.first(),str) && less(elementsList.last(),str))
        {
            elementsList.prepend(str);
            indexOfLastAdded=0;
        }
        else
        {
            int a=0;
            int b=elementsList.size()-1;

            while((b-a-1) > 0)
            {
                int mid=(b-a)/2;

                if(ge(elementsList.at(a),str) && less(elementsList.at(a+mid),str))
                   b=a+mid;
                else
                   a=a+mid;
            }
            elementsList.insert(b,str);
            indexOfLastAdded=b;
        }
    }
}

Storage::Storage()
{

}

void Storage::add(QString str)
{
    QCollator lexCollator;

    lexCollator.setCaseSensitivity(Qt::CaseInsensitive);
    lexCollator.setIgnorePunctuation(true);

    QCollator natCollator;

    natCollator.setCaseSensitivity(Qt::CaseInsensitive);
    natCollator.setIgnorePunctuation(true);
    natCollator.setNumericMode(true);

    if(str != "")
    {
        if(sortType==ST_NONE || elementsList.size()==0)
        {
            elementsList.append(str);
            indexOfLastAdded=elementsList.size()-1;
            emit listChanged(elementsList);
        }
        else
        {
            if(sortOrder==SO_ASC)
            {
                if(sortType==ST_LEXIC)
                    addAccordingToAscSort(str,[&](const QString &a, const QString &b){return lexCollator.compare(a,b)<0;},[&](const QString &a, const QString &b){return lexCollator.compare(a,b)>=0;});
                else
                    addAccordingToAscSort(str,[&](const QString &a, const QString &b){return natCollator.compare(a,b)<0;},[&](const QString &a, const QString &b){return natCollator.compare(a,b)>=0;});
                emit listChanged(elementsList);
            }
            else
            {
                if(sortType==ST_LEXIC)
                    addAccordingToDescSort(str,[&](const QString &a, const QString &b){return lexCollator.compare(a,b)>=0;},[&](const QString &a, const QString &b){return lexCollator.compare(a,b)<0;});
                else
                    addAccordingToDescSort(str,[&](const QString &a, const QString &b){return natCollator.compare(a,b)>=0;},[&](const QString &a, const QString &b){return natCollator.compare(a,b)<0;});
                emit listChanged(elementsList);
            }
        }
    }
    emit notifyListViewAboutAdd(indexOfLastAdded);
}

void Storage::remove(int row)
{
    elementsList.removeAt(row);
    emit listChanged(elementsList);
}

void Storage::clear()
{
    elementsList.clear();
    emit listChanged(elementsList);
}

void Storage::save(Filter filter)
{
    QString filename = QFileDialog::getSaveFileName(nullptr,"Сохранить список",".","Список (*.lbx)");
    QFile save(filename);
    if(save.open(QIODevice::WriteOnly))
    {
       QTextStream out(&save);
       out << sortOrder << "\r\n" << sortType << "\r\n" << filter << "\r\n";
       for(auto &s:elementsList)
           out << s << "\r\n";
       save.close();
    }

}

void Storage::load(QString filename, SortType sortType, SortOrder sortOrder)
{
    elementsList.clear();
    QFile load(filename);
    QString t;
    QTextStream in(&load);
    if(load.open(QIODevice::ReadOnly))
    {
        for(int i=0;i<3;i++)
            in.readLineInto(&t);

        while(in.readLineInto(&t))
            if(t!="")
            elementsList.append(t);

        this->sortType=sortType;
        this->sortOrder=sortOrder;

        load.close();
    }

    performSorting();

    emit listChanged(elementsList);
}

void Storage::setSortOrder(SortOrder sortOrder)
{
    this->sortOrder=sortOrder;

    if(performSorting())
        emit listChanged(elementsList);
}

void Storage::setSortType(SortType sortType)
{
    this->sortType = sortType;

    if(performSorting())
        emit listChanged(elementsList);
}
