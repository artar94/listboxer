/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *m_entryQuit;
    QAction *m_entrySearch;
    QAction *m_entryClear;
    QAction *m_entryAbout;
    QAction *m_entryAboutQt;
    QAction *m_entrySave;
    QAction *m_entryOpen;
    QWidget *FormWidget;
    QWidget *layoutWidget;
    QVBoxLayout *SortTypeLayout;
    QLabel *lbSortType;
    QRadioButton *rbNone;
    QRadioButton *rbLex;
    QRadioButton *rbNatural;
    QWidget *layoutWidget1;
    QVBoxLayout *SortOrderLayout;
    QLabel *lbSortOrder;
    QRadioButton *rbDesc;
    QRadioButton *rbAsc;
    QListView *listView;
    QFrame *line;
    QFrame *line_2;
    QWidget *layoutWidget2;
    QVBoxLayout *FiltersLayout;
    QLabel *lbFilters;
    QRadioButton *rbAll;
    QRadioButton *rbRussian;
    QRadioButton *rbEnglish;
    QRadioButton *rbRusEngPunct;
    QLineEdit *leListBoxElement;
    QPushButton *btnAdd;
    QPushButton *btnDelete;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuHelp;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(879, 505);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(879, 505));
        MainWindow->setMaximumSize(QSize(879, 505));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/icons/ListBoxerAppIcon.svg"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        m_entryQuit = new QAction(MainWindow);
        m_entryQuit->setObjectName(QStringLiteral("m_entryQuit"));
        m_entrySearch = new QAction(MainWindow);
        m_entrySearch->setObjectName(QStringLiteral("m_entrySearch"));
        m_entryClear = new QAction(MainWindow);
        m_entryClear->setObjectName(QStringLiteral("m_entryClear"));
        m_entryAbout = new QAction(MainWindow);
        m_entryAbout->setObjectName(QStringLiteral("m_entryAbout"));
        m_entryAboutQt = new QAction(MainWindow);
        m_entryAboutQt->setObjectName(QStringLiteral("m_entryAboutQt"));
        m_entrySave = new QAction(MainWindow);
        m_entrySave->setObjectName(QStringLiteral("m_entrySave"));
        m_entryOpen = new QAction(MainWindow);
        m_entryOpen->setObjectName(QStringLiteral("m_entryOpen"));
        FormWidget = new QWidget(MainWindow);
        FormWidget->setObjectName(QStringLiteral("FormWidget"));
        FormWidget->setEnabled(true);
        layoutWidget = new QWidget(FormWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 120, 181, 101));
        SortTypeLayout = new QVBoxLayout(layoutWidget);
        SortTypeLayout->setSpacing(6);
        SortTypeLayout->setContentsMargins(11, 11, 11, 11);
        SortTypeLayout->setObjectName(QStringLiteral("SortTypeLayout"));
        SortTypeLayout->setContentsMargins(0, 0, 0, 0);
        lbSortType = new QLabel(layoutWidget);
        lbSortType->setObjectName(QStringLiteral("lbSortType"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        lbSortType->setFont(font);

        SortTypeLayout->addWidget(lbSortType);

        rbNone = new QRadioButton(layoutWidget);
        rbNone->setObjectName(QStringLiteral("rbNone"));
        rbNone->setLayoutDirection(Qt::LeftToRight);
        rbNone->setCheckable(true);
        rbNone->setChecked(true);

        SortTypeLayout->addWidget(rbNone);

        rbLex = new QRadioButton(layoutWidget);
        rbLex->setObjectName(QStringLiteral("rbLex"));

        SortTypeLayout->addWidget(rbLex);

        rbNatural = new QRadioButton(layoutWidget);
        rbNatural->setObjectName(QStringLiteral("rbNatural"));

        SortTypeLayout->addWidget(rbNatural);

        layoutWidget1 = new QWidget(FormWidget);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 10, 181, 81));
        SortOrderLayout = new QVBoxLayout(layoutWidget1);
        SortOrderLayout->setSpacing(6);
        SortOrderLayout->setContentsMargins(11, 11, 11, 11);
        SortOrderLayout->setObjectName(QStringLiteral("SortOrderLayout"));
        SortOrderLayout->setContentsMargins(0, 0, 0, 0);
        lbSortOrder = new QLabel(layoutWidget1);
        lbSortOrder->setObjectName(QStringLiteral("lbSortOrder"));
        lbSortOrder->setFont(font);

        SortOrderLayout->addWidget(lbSortOrder);

        rbDesc = new QRadioButton(layoutWidget1);
        rbDesc->setObjectName(QStringLiteral("rbDesc"));
        rbDesc->setEnabled(false);
        rbDesc->setCheckable(true);
        rbDesc->setChecked(false);

        SortOrderLayout->addWidget(rbDesc);

        rbAsc = new QRadioButton(layoutWidget1);
        rbAsc->setObjectName(QStringLiteral("rbAsc"));
        rbAsc->setEnabled(false);
        rbAsc->setLayoutDirection(Qt::LeftToRight);
        rbAsc->setChecked(true);

        SortOrderLayout->addWidget(rbAsc);

        listView = new QListView(FormWidget);
        listView->setObjectName(QStringLiteral("listView"));
        listView->setGeometry(QRect(360, 10, 501, 393));
        listView->setAcceptDrops(false);
        listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        listView->setSelectionMode(QAbstractItemView::SingleSelection);
        line = new QFrame(FormWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(10, 400, 330, 7));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(FormWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(330, 10, 21, 393));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        layoutWidget2 = new QWidget(FormWidget);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(10, 250, 331, 131));
        FiltersLayout = new QVBoxLayout(layoutWidget2);
        FiltersLayout->setSpacing(6);
        FiltersLayout->setContentsMargins(11, 11, 11, 11);
        FiltersLayout->setObjectName(QStringLiteral("FiltersLayout"));
        FiltersLayout->setContentsMargins(0, 0, 0, 0);
        lbFilters = new QLabel(layoutWidget2);
        lbFilters->setObjectName(QStringLiteral("lbFilters"));
        lbFilters->setFont(font);

        FiltersLayout->addWidget(lbFilters);

        rbAll = new QRadioButton(layoutWidget2);
        rbAll->setObjectName(QStringLiteral("rbAll"));
        rbAll->setCheckable(true);
        rbAll->setChecked(true);

        FiltersLayout->addWidget(rbAll);

        rbRussian = new QRadioButton(layoutWidget2);
        rbRussian->setObjectName(QStringLiteral("rbRussian"));

        FiltersLayout->addWidget(rbRussian);

        rbEnglish = new QRadioButton(layoutWidget2);
        rbEnglish->setObjectName(QStringLiteral("rbEnglish"));

        FiltersLayout->addWidget(rbEnglish);

        rbRusEngPunct = new QRadioButton(layoutWidget2);
        rbRusEngPunct->setObjectName(QStringLiteral("rbRusEngPunct"));

        FiltersLayout->addWidget(rbRusEngPunct);

        leListBoxElement = new QLineEdit(FormWidget);
        leListBoxElement->setObjectName(QStringLiteral("leListBoxElement"));
        leListBoxElement->setGeometry(QRect(360, 430, 501, 31));
        leListBoxElement->setContextMenuPolicy(Qt::NoContextMenu);
        leListBoxElement->setMaxLength(256);
        btnAdd = new QPushButton(FormWidget);
        btnAdd->setObjectName(QStringLiteral("btnAdd"));
        btnAdd->setGeometry(QRect(189, 430, 151, 31));
        btnAdd->setFlat(false);
        btnDelete = new QPushButton(FormWidget);
        btnDelete->setObjectName(QStringLiteral("btnDelete"));
        btnDelete->setGeometry(QRect(9, 430, 141, 31));
        MainWindow->setCentralWidget(FormWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 879, 17));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(menuBar->sizePolicy().hasHeightForWidth());
        menuBar->setSizePolicy(sizePolicy1);
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(m_entryOpen);
        menuFile->addSeparator();
        menuFile->addAction(m_entrySave);
        menuFile->addSeparator();
        menuFile->addAction(m_entryQuit);
        menuEdit->addAction(m_entrySearch);
        menuEdit->addSeparator();
        menuEdit->addAction(m_entryClear);
        menuHelp->addSeparator();
        menuHelp->addAction(m_entryAbout);
        menuHelp->addSeparator();
        menuHelp->addAction(m_entryAboutQt);

        retranslateUi(MainWindow);
        QObject::connect(rbNone, SIGNAL(toggled(bool)), rbDesc, SLOT(setDisabled(bool)));
        QObject::connect(rbNone, SIGNAL(toggled(bool)), rbAsc, SLOT(setDisabled(bool)));
        QObject::connect(m_entryQuit, SIGNAL(triggered()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ListBoxer", 0));
        m_entryQuit->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0));
        m_entryQuit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", 0));
        m_entrySearch->setText(QApplication::translate("MainWindow", "\320\237\320\276\320\270\321\201\320\272", 0));
        m_entrySearch->setShortcut(QApplication::translate("MainWindow", "Ctrl+F", 0));
        m_entryClear->setText(QApplication::translate("MainWindow", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", 0));
        m_entryClear->setShortcut(QApplication::translate("MainWindow", "Ctrl+W", 0));
        m_entryAbout->setText(QApplication::translate("MainWindow", "\320\236 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0));
        m_entryAboutQt->setText(QApplication::translate("MainWindow", "O Qt", 0));
        m_entrySave->setText(QApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0));
        m_entrySave->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0));
        m_entryOpen->setText(QApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214", 0));
        m_entryOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        lbSortType->setText(QApplication::translate("MainWindow", "\320\241\320\276\321\200\321\202\320\270\321\200\320\276\320\262\320\272\320\260:", 0));
        rbNone->setText(QApplication::translate("MainWindow", "\320\235\320\265\321\202", 0));
        rbLex->setText(QApplication::translate("MainWindow", "\320\233\320\265\320\272\321\201\320\270\320\272\320\276\320\263\321\200\320\260\321\204\320\270\321\207\320\265\321\201\320\272\320\260\321\217", 0));
        rbNatural->setText(QApplication::translate("MainWindow", "\320\225\321\201\321\202\320\265\321\201\321\202\320\262\320\265\320\275\320\275\320\260\321\217", 0));
        lbSortOrder->setText(QApplication::translate("MainWindow", "\320\237\320\276\321\200\321\217\320\264\320\276\320\272 \321\201\320\276\321\200\321\202\320\270\321\200\320\276\320\262\320\272\320\270:", 0));
        rbDesc->setText(QApplication::translate("MainWindow", "\320\243\320\261\321\213\320\262\320\260\321\216\321\211\320\260\321\217", 0));
        rbAsc->setText(QApplication::translate("MainWindow", "\320\222\320\276\320\267\321\200\320\260\321\201\321\202\320\260\321\216\321\211\320\260\321\217", 0));
        lbFilters->setText(QApplication::translate("MainWindow", "\320\244\320\270\320\273\321\214\321\202\321\200\321\213:", 0));
        rbAll->setText(QApplication::translate("MainWindow", "\320\222\321\201\321\221", 0));
        rbRussian->setText(QApplication::translate("MainWindow", "\320\240\321\203\321\201\321\201\320\272\320\270\320\271 \320\260\320\273\321\204\320\260\320\262\320\270\321\202", 0));
        rbEnglish->setText(QApplication::translate("MainWindow", "\320\220\320\275\320\263\320\273\320\270\320\271\321\201\320\272\320\270\320\271 \320\260\320\273\321\204\320\260\320\262\320\270\321\202", 0));
        rbRusEngPunct->setText(QApplication::translate("MainWindow", "\320\220\320\275\320\263\320\273\320\270\320\271\321\201\320\272\320\270\320\271, \321\200\321\203\321\201\321\201\320\272\320\270\320\271, \320\277\321\203\320\275\320\272\321\202\321\203\320\260\321\206\320\270\321\217", 0));
        btnAdd->setText(QApplication::translate("MainWindow", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0));
        btnDelete->setText(QApplication::translate("MainWindow", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "\320\244\320\260\320\271\320\273", 0));
        menuEdit->setTitle(QApplication::translate("MainWindow", "\320\237\321\200\320\260\320\262\320\272\320\260", 0));
        menuHelp->setTitle(QApplication::translate("MainWindow", "\320\237\320\276\320\274\320\276\321\211\321\214", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
