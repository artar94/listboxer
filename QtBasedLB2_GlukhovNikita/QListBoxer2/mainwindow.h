#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include <listboxerdefs.h>
#include <QList>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

public slots:
    void refreshListView(const QStringList&);
    void performSelectAndScroll(int);

private slots:
    void on_btnAdd_clicked();

    void on_listView_doubleClicked(const QModelIndex&);

    void on_btnDelete_clicked();

    void on_rbLex_clicked();

    void on_rbNatural_clicked();

    void on_rbNone_clicked();

    void on_rbDesc_clicked();

    void on_rbAsc_clicked();

    void on_m_entryOpen_triggered();

    void on_m_entrySave_triggered();

    void on_m_entrySearch_triggered();

    void on_m_entryClear_triggered();

    void on_m_entryAboutQt_triggered();

    void on_m_entryAbout_triggered();

    void on_leListBoxElement_returnPressed();

    void on_rbRussian_clicked();

    void on_rbAll_clicked();

    void on_rbEnglish_clicked();

    void on_rbRusEngPunct_clicked();

signals:
    void addToListActionPerformed(QString);
    void removeFromListActionPerformed(int);
    void sortOrderChosen(SortOrder);
    void sortTypeChosen(SortType);
    void listClearRequested();
    void saveListRequested(Filter);
    void loadListRequested(QString, SortType, SortOrder);

private:
    Ui::MainWindow *ui;
    QStringListModel model;
    QList<int> hiddenRows;
    QList<int> visibleRows;
    Filter filter = F_ALL;

    void unhide();
    void hide(bool (*)(QString));
    void refilter();
    Filter getStringFilterClass(QString);
};

#endif // MAINWINDOW_H
