#ifndef LISTBOXERDEFS_H
#define LISTBOXERDEFS_H

enum SortType {ST_NONE, ST_LEXIC, ST_NATURAL};
enum SortOrder {SO_ASC,SO_DESC};
enum Filter {F_ALL, F_RUS, F_ENG, F_RUS_ENG_PUNCT};

#endif // LISTBOXERDEFS_H
