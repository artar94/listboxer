#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QRadioButton>

class FindDialog: public QDialog
{
    Q_OBJECT
public:
    FindDialog(QWidget* parent = Q_NULLPTR);

    QString searchMask() const;

    bool isSearchDownward() const;

private:
    QLineEdit* leSearchMask;
    QRadioButton* rbUp;
    QRadioButton* rbDown;
};

#endif // FINDDIALOG_H
