#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <QStringList>
#include <listboxerdefs.h>
#include <functional>

class Storage: public QObject
{
    Q_OBJECT

private:
    SortType sortType = ST_NONE;
    SortOrder sortOrder = SO_ASC;
    QStringList elementsList;
    int indexOfLastAdded;

    bool performSorting();
    void addAccordingToAscSort(const QString&, std::function<bool (const QString&, const QString&)>, std::function<bool (const QString&, const QString&)>);
    void addAccordingToDescSort(const QString&, std::function<bool (const QString&, const QString&)>, std::function<bool (const QString&, const QString&)>);

public:
    Storage();

public slots:
    void add(QString);
    void remove(int);
    void clear();
    void save(Filter);
    void load(QString, SortType, SortOrder);
    void setSortOrder(SortOrder);
    void setSortType(SortType);

signals:
    void listChanged(const QStringList&);
    void notifyListViewAboutAdd(int);

};

#endif // STORAGE_H
