#include "mainwindow.h"
#include <QApplication>
#include <storage.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Storage s;
    MainWindow w;

    QObject::connect(&w, &MainWindow::addToListActionPerformed, &s, &Storage::add);
    QObject::connect(&s, &Storage::listChanged, &w, &MainWindow::refreshListView);
    QObject::connect(&w, &MainWindow::removeFromListActionPerformed, &s, &Storage::remove);
    QObject::connect(&w, &MainWindow::sortTypeChosen, &s, &Storage::setSortType);
    QObject::connect(&w, &MainWindow::sortOrderChosen, &s, &Storage::setSortOrder);
    QObject::connect(&w, &MainWindow::listClearRequested, &s, &Storage::clear);
    QObject::connect(&s, &Storage::notifyListViewAboutAdd, &w, &MainWindow::performSelectAndScroll);
    QObject::connect(&w, &MainWindow::saveListRequested, &s, &Storage::save);
    QObject::connect(&w, &MainWindow::loadListRequested, &s, &Storage::load);

    w.show();

    return a.exec();
}
