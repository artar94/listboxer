#include "sortassistant.h"
#include <QCollator>
#include <algorithm>

SortAssistant::SortAssistant()
{

}

void SortAssistant::sortLexAsc(QStringList &str)
{
    str.sort(Qt::CaseInsensitive);
}

void SortAssistant::sortLexDesc(QStringList &str)
{
    QCollator col;

    col.setCaseSensitivity(Qt::CaseInsensitive);
    col.setIgnorePunctuation(true);

    std::sort(str.begin(), str.end(),[&](const QString &a,const QString &b){return col.compare(a,b) > 0;});
}

void SortAssistant::sortNatAsc(QStringList &str)
{
    QCollator col;

    col.setCaseSensitivity(Qt::CaseInsensitive);
    col.setIgnorePunctuation(true);
    col.setNumericMode(true);

    std::sort(str.begin(), str.end(),[&](const QString &a,const QString &b){return col.compare(a,b) < 0;});
}

void SortAssistant::sortNatDesc(QStringList &str)
{
    QCollator col;

    col.setCaseSensitivity(Qt::CaseInsensitive);
    col.setIgnorePunctuation(true);
    col.setNumericMode(true);

    std::sort(str.begin(), str.end(),[&](const QString &a,const QString &b){return col.compare(a,b) > 0;});
}
