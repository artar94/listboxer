#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "filterassistant.h"
#include <QMessageBox>
#include <QTextStream>
#include <QKeyEvent>
#include <QClipboard>
#include <QFileDialog>
#include <QRegularExpressionMatch>
#include "finddialog.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    model.setStringList(QStringList());
    ui->setupUi(this);
    ui->listView->setModel(&model);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(ui->listView->hasFocus() && !ui->listView->selectionModel()->selectedIndexes().empty() &&  event->matches(QKeySequence::Copy))
        QApplication::clipboard()->setText(ui->listView->selectionModel()->selectedIndexes().first().data().toString());
    else
        QMainWindow::keyPressEvent(event);
}

void MainWindow::on_btnAdd_clicked()
{
    emit addToListActionPerformed(ui->leListBoxElement->text().simplified());
    ui->leListBoxElement->clear();
}

void MainWindow::refreshListView(const QStringList &list)
{
    model.setStringList(list);
    ui->listView->setModel(&model);
    refilter();
}

void MainWindow::performSelectAndScroll(int index)
{
    QModelIndex rowToSelect = model.index(index);
    if(getStringFilterClass(rowToSelect.data().toString())==filter || filter==F_ALL)
        ui->listView->setCurrentIndex(rowToSelect);
}

void MainWindow::on_listView_doubleClicked(const QModelIndex &index)
{
    ui->leListBoxElement->clear();
    ui->leListBoxElement->insert(index.data().toString());
}

void MainWindow::on_btnDelete_clicked()
{
    const QModelIndexList selectedItem = ui->listView->selectionModel()->selectedIndexes();

    foreach(const QModelIndex &index, selectedItem)
    {
        emit removeFromListActionPerformed(index.row());
    }
}
void MainWindow::on_rbLex_clicked()
{
    emit sortTypeChosen(ST_LEXIC);
}

void MainWindow::on_rbNatural_clicked()
{
    emit sortTypeChosen(ST_NATURAL);
}

void MainWindow::on_rbNone_clicked()
{
    emit sortTypeChosen(ST_NONE);
}

void MainWindow::on_rbDesc_clicked()
{
    emit sortOrderChosen(SO_DESC);
}

void MainWindow::on_rbAsc_clicked()
{
    emit sortOrderChosen(SO_ASC);
}

void MainWindow::on_m_entryOpen_triggered()
{
    SortOrder sO;
    SortType sT;

    auto setSortType = [this](SortType sortType){
        switch(sortType)
        {
        case ST_NONE:{this->ui->rbNone->setChecked(true); break;}
        case ST_LEXIC:{this->ui->rbLex->setChecked(true);break;}
        case ST_NATURAL:{this->ui->rbNatural->setChecked(true);break;}
        }
    };

    auto setSortOrder = [this](SortOrder sortOrder){
        switch(sortOrder)
        {
        case SO_ASC:{this->ui->rbAsc->setChecked(true);break;}
        case SO_DESC:{this->ui->rbDesc->setChecked(true);break;}
        }
    };

    auto setFilter = [this](Filter filter){
        switch(filter)
        {
        case F_ALL:{this->filter=filter; this->ui->rbAll->setChecked(true); break;}
        case F_RUS:{this->filter=filter; this->ui->rbRussian->setChecked(true); break;}
        case F_ENG:{this->filter=filter; this->ui->rbEnglish->setChecked(true); break;}
        case F_RUS_ENG_PUNCT:{this->filter=filter; this->ui->rbRusEngPunct->setChecked(true); break;}
        }
    };

    QString filename = QFileDialog::getOpenFileName(nullptr,"Открыть",".","Список (*.lbx)");
    QFile load(filename);
    if(load.open(QIODevice::ReadOnly))
    {
       QTextStream in(&load);
       QString t;

       if(in.readLineInto(&t) && t != "")
       {
          bool ok;
          int tmp = t.toInt(&ok);
          if(ok)
          {
              sO=(SortOrder)(tmp);
              setSortOrder(sO);
          }
       }
       else
       {
          load.close();
          QMessageBox::critical(this,"Ошибка","Был неожиданно достигнут конец файла!");
          return;
       }

       if(in.readLineInto(&t) && t != "")
       {
           bool ok;
           int tmp = t.toInt(&ok);
           if(ok)
           {
               sT=(SortType)(tmp);
               setSortType(sT);
           }
       }
       else
       {
          load.close();
          QMessageBox::critical(this,"Ошибка","Был неожиданно достигнут конец файла!");
          return;
       }

       if(in.readLineInto(&t) && t != "")
       {
           bool ok;
           int tmp = t.toInt(&ok);
           if(ok)
           {
               setFilter((Filter)tmp);
           }
       }
       else
       {
          load.close();
          QMessageBox::critical(this,"Ошибка","Был неожиданно достигнут конец файла, либо файл не соответствует формату сохранений ListBoxer!");
          return;
       }

       load.close();
    }

    emit loadListRequested(filename,sT, sO);
}

void MainWindow::on_m_entrySave_triggered()
{
    emit saveListRequested(filter);

}

void MainWindow::on_m_entrySearch_triggered()
{
    QString sMask;

    FindDialog* fDialog = new FindDialog(this);

    fDialog->setWindowTitle("Найти");

    if(fDialog->exec()==QDialog::Accepted)
    {
        sMask.append(fDialog->searchMask());
    }

    if(sMask!="")
    {
        auto f = [](QString s, QString c)
        {
         s.prepend("^");
         QRegularExpression re(s);
         QRegularExpressionMatch match = re.match(c);
         if(match.hasMatch())
             return true;
         else
             return false;
        };

        QModelIndex index;

        if(!visibleRows.empty())
        {
            if(ui->listView->selectionModel()->selectedIndexes().empty())
            {
                if(fDialog->isSearchDownward())
                    for(int i:visibleRows)
                    {
                    index = model.index(i);
                    if(f(sMask,index.data().toString()))
                    {
                        ui->listView->setCurrentIndex(index);
                        break;
                    }
                    }
                else
                    for(auto it = visibleRows.rbegin();it!=visibleRows.rend();it++)
                    {
                        index = model.index(*it);
                        if(f(sMask,index.data().toString()))
                        {
                            ui->listView->setCurrentIndex(index);
                            break;
                        }
                    }
            }
            else
            {
                ui->listView->setCurrentIndex(ui->listView->selectionModel()->selectedIndexes().first());
                if(fDialog->isSearchDownward())
                    for(int i=visibleRows.indexOf(ui->listView->currentIndex().row())+1;i<visibleRows.size();i++)
                    {
                        index = model.index(visibleRows.at(i));
                        if(f(sMask,index.data().toString()))
                        {
                            ui->listView->setCurrentIndex(index);
                            break;
                        }
                    }
                else
                    for(int i=visibleRows.indexOf(ui->listView->currentIndex().row())-1;i>=0;i--)
                    {
                        index = model.index(visibleRows.at(i));
                        if(f(sMask,index.data().toString()))
                        {
                            ui->listView->setCurrentIndex(index);
                            break;
                        }
                    }
            }
        }
        else
        {
            if(ui->listView->selectionModel()->selectedIndexes().empty())
            {
            if(fDialog->isSearchDownward())
            for(int i=0;i<model.rowCount();i++)
            {
                index = model.index(i);
                if(f(sMask,index.data().toString()))
                {
                    ui->listView->setCurrentIndex(index);
                    break;
                }
            }
            else
                for(int i=model.rowCount()-1;i>=0;i--)
                {
                    index = model.index(i);
                    if(f(sMask,index.data().toString()))
                    {
                        ui->listView->setCurrentIndex(index);
                        break;
                    }
                }
            }
            else
            {
                ui->listView->setCurrentIndex(ui->listView->selectionModel()->selectedIndexes().first());
                if(fDialog->isSearchDownward())
                    for(int i=ui->listView->currentIndex().row()+1;i<model.rowCount();i++)
                    {
                        index = model.index(i);
                        if(f(sMask,index.data().toString()))
                        {
                            ui->listView->setCurrentIndex(index);
                            break;
                        }
                    }
                else
                    for(int i=ui->listView->currentIndex().row()-1;i>=0;i--)
                    {
                        index = model.index(i);
                        if(f(sMask,index.data().toString()))
                        {
                            ui->listView->setCurrentIndex(index);
                            break;
                        }
                    }
              }

        }

    }

}

void MainWindow::on_m_entryClear_triggered()
{
    emit listClearRequested();
}

void MainWindow::on_m_entryAboutQt_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_m_entryAbout_triggered()
{
    QMessageBox::about(this,"О программе","Разработчик: Глухов Никита Святославович\nВГУ, ПММ, кафедра МОЭВМ, магистратура\n1 курс, 13 группа\nПредмет: тестирование ПО");
}

void MainWindow::on_leListBoxElement_returnPressed()
{
    emit addToListActionPerformed(ui->leListBoxElement->text());
    ui->leListBoxElement->clear();
}

void MainWindow::on_rbRussian_clicked()
{
    filter = F_RUS;
    unhide();
    hide(FilterAssistant::filterRusValidator);

}

void MainWindow::unhide()
{
    visibleRows.clear();
    if(!hiddenRows.isEmpty())
    {
       for(int i:hiddenRows)
       {
           ui->listView->setRowHidden(i,false);
       }
       hiddenRows.clear();
    }
}

void MainWindow::hide(bool (*v)(QString))
{
    unhide();
    for(int i=0;i<model.rowCount();i++)
    {
        if(!v(model.index(i).data().toString()))
        {
            ui->listView->setRowHidden(i,true);
            hiddenRows.append(i);
        }
        else
            visibleRows.append(i);
    }
}

void MainWindow::refilter()
{
    if(filter==F_RUS)
        hide(FilterAssistant::filterRusValidator);
    else if(filter==F_ENG)
        hide(FilterAssistant::filterEngValidator);
    else if(filter==F_RUS_ENG_PUNCT)
        hide(FilterAssistant::filterRusEngPunctValidator);
    else
        unhide();
}

Filter MainWindow::getStringFilterClass(QString str)
{
    if(FilterAssistant::filterRusValidator(str))
        return F_RUS;
    else if(FilterAssistant::filterEngValidator(str))
        return F_ENG;
    else if(FilterAssistant::filterRusEngPunctValidator(str))
        return F_RUS_ENG_PUNCT;
    else
        return F_ALL;
}

void MainWindow::on_rbAll_clicked()
{
    filter = F_ALL;
    unhide();
}

void MainWindow::on_rbEnglish_clicked()
{
    filter = F_ENG;
    unhide();
    hide(FilterAssistant::filterEngValidator);
}

void MainWindow::on_rbRusEngPunct_clicked()
{
    filter = F_RUS_ENG_PUNCT;
    unhide();
    hide(FilterAssistant::filterRusEngPunctValidator);
}
