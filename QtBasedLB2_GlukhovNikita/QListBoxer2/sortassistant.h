#ifndef SORTASSISTANT_H
#define SORTASSISTANT_H
#include <QStringList>

class SortAssistant
{
private:
    SortAssistant();
public:
    static void sortLexAsc(QStringList &str); // Лексикографическя сортировка по возрастанию
    static void sortLexDesc(QStringList &str); // Лексикографическая сортировка по убыванию
    static void sortNatAsc(QStringList &str); // Естественная сортировка по возрастанию
    static void sortNatDesc(QStringList &str); // Естественная сортировка по убыванию
};

#endif // SORTASSISTANT_H
