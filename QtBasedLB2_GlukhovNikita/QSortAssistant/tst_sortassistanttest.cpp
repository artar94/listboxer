#include <QStringList>
#include <QtTest>
#include "sortassistant.h"

class SortAssistantTest : public QObject
{
    Q_OBJECT

public:
    SortAssistantTest();

private Q_SLOTS:
    void sortLexAsc();
    void sortLexDesc();
    void sortNatAsc();
    void sortNatDesc();
};

SortAssistantTest::SortAssistantTest()
{
}

void SortAssistantTest::sortLexAsc()
{
    QStringList str1;
    str1 << "ddd" << "aaa" << "bbb" << "ccc";
    QStringList str2;
    str2 << "aaa" << "bbb" << "ccc" << "ddd";

    SortAssistant::sortLexAsc(str1);

    QCOMPARE(str1,str2);
}

void SortAssistantTest::sortLexDesc()
{
    QStringList str1;
    str1 << "ddd" << "aaa" << "bbb" << "ccc";
    QStringList str2;
    str2 << "ddd" << "ccc" << "bbb" << "aaa";

    SortAssistant::sortLexDesc(str1);

    QCOMPARE(str1,str2);
}

void SortAssistantTest::sortNatAsc()
{
    QStringList str1;
    str1 << "file10" << "file2" << "file22";
    QStringList str2;
    str2 << "file2" << "file10" << "file22";

    SortAssistant::sortNatAsc(str1);

    QCOMPARE(str1,str2);
}

void SortAssistantTest::sortNatDesc()
{
    QStringList str1;
    str1 << "file15" << "file92" << "file3" << "file0";
    QStringList str2;
    str2 << "file92" << "file15" << "file3" << "file0";

    SortAssistant::sortNatDesc(str1);

    QCOMPARE(str1,str2);
}

QTEST_APPLESS_MAIN(SortAssistantTest)

#include "tst_sortassistanttest.moc"
