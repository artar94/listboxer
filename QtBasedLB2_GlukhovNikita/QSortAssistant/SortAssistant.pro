#-------------------------------------------------
#
# Project created by QtCreator 2016-04-19T12:00:24
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_sortassistanttest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += \
    tst_sortassistanttest.cpp \
    sortassistant.cpp

HEADERS += \
    sortassistant.h
